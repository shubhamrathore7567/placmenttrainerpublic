//MAde by Namjoshi 09/06/21

import java.rmi.*;
import java.rmi.server.*;
import java.sql.*;
import java.util.*;
import javax.servlet.http.*;


public class RegisterRemote extends UnicastRemoteObject implements RegisterInterface
{

    RegisterRemote() throws RemoteException
    {
        super();
    } 

    public boolean register(User user) throws RemoteException
    {
         //for jdbc 
        
        boolean isStudent=false;
        
       
        try 
        {
            String JDBC_DRIVER ="com.mysql.jdbc.Driver";
            String DB_URL = "jdbc:mysql://localhost/PlacementTrainer_DB";
       
            Connection con = null;
            Statement stmt = null;
            String USER="root";
            String PASS="05Jan1999";

          
            Class.forName(JDBC_DRIVER);
           
            con = DriverManager.getConnection(DB_URL,USER,PASS);
        
          
            stmt=con.createStatement();
            
         

            PreparedStatement p=con.prepareStatement("insert into UserMaster(username,name,password,email,contact,userRole) values(?,?,?,?,?,?)");
		

			p.setString(1,user.getUserName());
	        p.setString(2,user.getName());
            p.setString(3,user.getPassword());
            p.setString(4,user.getEmail());
            p.setString(5,user.getContact());

            if(user.getUserRole().equals("Student")) 
                p.setString(6,"Student");
             
            else
                p.setString(6,"Mentor");
            
              
		
		    int i = p.executeUpdate();
		    System.out.print("\n"+i+" row inserted successfully!");

            
            stmt=con.createStatement();      
            String query="Select userId from usermaster where username=\""+user.getUserName()+"\"";
            ResultSet rs = stmt.executeQuery(query);

            rs.next();
            String uid=rs.getString("userId");
            user.setUserId(Integer.parseInt(uid));

            if(isStudent)
            {
                p=con.prepareStatement("insert into studentprofile(userid,organization,stream) values(?,?,?)");
                p.setString(1,uid);
	            p.setString(2,user.getOrganization());
                p.setString(3,user.getStream());
            }
            else
            {
                p=con.prepareStatement("insert into mentorprofile(userid,skills) values(?,?)");
                p.setString(1,uid);
	            p.setString(2,user.getSkills());
            }
            int i2 = p.executeUpdate();
            
            return true;
           
        }  catch (Exception e) {
            System.out.println(e.getMessage());
            e.printStackTrace();
            return false;
            //TODO: handle exception
        } 
    }
    public int getUserId(String username) throws RemoteException
    {
        try 
        {
            String JDBC_DRIVER ="com.mysql.jdbc.Driver";
            String DB_URL = "jdbc:mysql://localhost/PlacementTrainer_DB";
    
            Connection con = null;
            Statement stmt = null;
            String USER="root";
            String PASS="05Jan1999";

        
            Class.forName(JDBC_DRIVER);
        
            con = DriverManager.getConnection(DB_URL,USER,PASS);
        
        
            stmt=con.createStatement();
            
        
            String query="Select userId from usermaster where username=\""+username+"\"";
        
            ResultSet rs = stmt.executeQuery(query);

            rs.next();
           
            int userId=rs.getInt("userId");  //add session code

            return userId;
        }
        catch (Exception e) {
            System.out.println(e.getMessage());
            e.printStackTrace();
            return 0;
            //TODO: handle exception
        } 
    }
    public String login(String username) throws RemoteException
    {
        try 
        {
            String JDBC_DRIVER ="com.mysql.jdbc.Driver";
            String DB_URL = "jdbc:mysql://localhost/PlacementTrainer_DB";
    
            Connection con = null;
            Statement stmt = null;
            String USER="root";
            String PASS="05Jan1999";

        
            Class.forName(JDBC_DRIVER);
        
            con = DriverManager.getConnection(DB_URL,USER,PASS);
        
        
            stmt=con.createStatement();
            
        
            String query="Select userId,password from usermaster where username=\""+username+"\"";
        
            ResultSet rs = stmt.executeQuery(query);

            rs.next();
           
            String uPassword=rs.getString("password");
            int userId=rs.getInt("userId");  //add session code

            


            return uPassword;

        }
        catch (Exception e) {
            System.out.println(e.getMessage());
            e.printStackTrace();
            return null;
            //TODO: handle exception
        } 
        
    }

    public boolean addQuestions(Question[] que,Assesment assestment) throws RemoteException
    {
        try 
        {
            String JDBC_DRIVER ="com.mysql.jdbc.Driver";
            String DB_URL = "jdbc:mysql://localhost/PlacementTrainer_DB";
       
            Connection con = null;
            Statement stmt = null;
            String USER="root";
            String PASS="05Jan1999";

          
            Class.forName(JDBC_DRIVER);
           
            con = DriverManager.getConnection(DB_URL,USER,PASS);
        
          
            stmt=con.createStatement();
            
         

            PreparedStatement p=con.prepareStatement("insert into Questions(AssesmentID,UserID,Question,Option1,Option2,Option3,Option4,Answer,Weightage) values(?,?,?,?,?,?,?,?,?);");
		
          /*   Date date=new java.util.Date();
            Date sqlDate=new java.sql.Date(date.getTime());
            java.sql.Timestamp sqlTime=new java.sql.Timestamp(date.getTime()); */
           

            for(int i=0;i<que.length;i++)
            {

                p.setInt(1,que[i].getAssesmentID());
                p.setInt(2,que[i].getUserID());
                p.setString(3,que[i].getQuestion());
                p.setString(4,que[i].getOp1());
                p.setString(5,que[i].getOp2());
                p.setString(6,que[i].getOp3());
                p.setString(7,que[i].getOp4());
                p.setInt(8,que[i].getAns());
                p.setDouble(9,que[i].getWeightage());
                int j= p.executeUpdate();
                System.out.print("\n"+j+" row inserted successfully!");

            }
            
            p=con.prepareStatement("insert into assesmentmaster(UserID,AssessmentTitle,TopicId,LangId,PassingPaercentage,Weightage) values(?,?,?,?,?,?);");
            
            p.setInt(1,assestment.getUserID());
            p.setString(2,assestment.getTitle());
            p.setInt(3,assestment.getTopicId());
            p.setInt(4,assestment.getLangId());
            p.setDouble(5,assestment.getPassingPercentage());
            p.setDouble(6,assestment.getWeightage());
            int i= p.executeUpdate();
            System.out.print("\n"+i+" row inserted successfully!");

			return true;

        }  catch (Exception e) {
            System.out.println(e.getMessage());
            e.printStackTrace();
            return false;
            //TODO: handle exception
        } 
    }
    public ArrayList getAssesment(int topicId) throws RemoteException
    {
        ArrayList<String> AssessmentTitle = new ArrayList<String>();

        try 
        {
            String JDBC_DRIVER ="com.mysql.jdbc.Driver";
            String DB_URL = "jdbc:mysql://localhost/PlacementTrainer_DB";
       
            Connection con = null;
            Statement stmt = null;
            String USER="root";
            String PASS="05Jan1999";
           // String query="";
          
            Class.forName(JDBC_DRIVER);
           
            con = DriverManager.getConnection(DB_URL,USER,PASS);
        
          
            stmt=con.createStatement();
        
            String query="Select AssessmentTitle from AssesmentMaster where TopicId="+ topicId;

            //PreparedStatement p=con.prepareStatement("Select AssessmentTitle from AssesmentMaster where TopicId=(?)");

			//p.setInt(1,topicId);
	        ResultSet rs = stmt.executeQuery(query);      

            int i=0;
           
            while (rs.next()) {
                AssessmentTitle.add(rs.getString("AssessmentTitle"));
            }
            return AssessmentTitle; 

        }  catch (Exception e) {
            System.out.println(e.getMessage() + "\t hello2");
            e.printStackTrace();
            return AssessmentTitle;
        }
    }
    public Assesment takeAssesment(int assesmentId) throws RemoteException
    {
        try 
        {
            String JDBC_DRIVER ="com.mysql.jdbc.Driver";
            String DB_URL = "jdbc:mysql://localhost/PlacementTrainer_DB";
       
            Connection con = null;
            Statement stmt = null;
            String USER="root";
            String PASS="05Jan1999";
           // String query="";
          
            Class.forName(JDBC_DRIVER);
           
            con = DriverManager.getConnection(DB_URL,USER,PASS);
        
          
            stmt=con.createStatement();
        
            String query="Select Question,Option1,Option2,Option3,Option4,Answer,Weightage from Questions where AssesmentID="+ assesmentId;

            //PreparedStatement p=con.prepareStatement("Select AssessmentTitle from AssesmentMaster where TopicId=(?)");

			//p.setInt(1,topicId);
	        ResultSet rs = stmt.executeQuery(query);      

            int i=0;
            ArrayList<Question> que = new ArrayList<Question>();
            Assesment asses=new Assesment();
           
            while (rs.next()) {
                que.add(new Question());

                que.get(i).setQuestion(rs.getString("Question"));
                que.get(i).setOp1(rs.getString("Option1"));
                que.get(i).setOp2(rs.getString("Option2"));
                que.get(i).setOp3(rs.getString("Option3"));
                que.get(i).setOp4(rs.getString("Option4"));
                que.get(i).setAns(Integer.parseInt(rs.getString("Answer")));
                que.get(i).setWeightage(Double.parseDouble(rs.getString("Weightage")));

                i++;
            }


            asses.setQuestion(que);

            query="Select AssessmentTitle,TopicId,PassingPaercentage,Weightage from AssesmentMaster where AssementID="+ assesmentId;

            rs = stmt.executeQuery(query);
            while (rs.next()) 
            {
                asses.setTitle(rs.getString("AssessmentTitle"));
                asses.setTopicId(Integer.parseInt(rs.getString("TopicId")));
                asses.setPassingPercentage(Double.parseDouble(rs.getString("PassingPaercentage")));
                asses.setWeightage(Double.parseDouble(rs.getString("Weightage")));
            }


            return asses; 

        }  catch (Exception e) {
            System.out.println(e.getMessage() + "\t hello2");
            e.printStackTrace();
            return null;
        }
    }


    
    //Made by shivangi 10/06/21 1 Am
    public ArrayList<ArrayList<String>> mantorListing(int sessionUserid) throws RemoteException
    {

        ArrayList<ArrayList<String>> arrayList = new ArrayList<ArrayList<String>>();
        try 
        {
            String JDBC_DRIVER ="com.mysql.jdbc.Driver";
            String DB_URL = "jdbc:mysql://localhost/PlacementTrainer_DB";
       
            Connection con = null;
            Statement stmt = null;
            String USER="root";
            String PASS="05Jan1999";

          
            Class.forName(JDBC_DRIVER);
           
            con = DriverManager.getConnection(DB_URL,USER,PASS);
        
          
            stmt=con.createStatement();
        
            String query="Select * from topicmaster where userid= "+ sessionUserid +" and isActive=" + 1;

            //PreparedStatement p=con.prepareStatement("Select * from topicmaster where userid=(?) and isActive =(?)");

			//p.setInt(1,sessionUserid);
	        ResultSet rs = stmt.executeQuery(query);
          

            int i=0;
            String topicId,languageId;
            String topicTitle,description,labelImage;
            
            while (rs.next()) {
                topicId = rs.getString("TopicId");
                languageId = rs.getString("LanguageId");    
                topicTitle = rs.getString("Title");
                description = rs.getString("Description");
                labelImage = rs.getString("LabelImage");
                arrayList.add(i, new ArrayList<String>(Arrays.asList(topicId,languageId,topicTitle,description,labelImage)));
            }
            
            return arrayList; 

        }  catch (Exception e) {
            System.out.println(e.getMessage() + "\t hello2");
            e.printStackTrace();
            return arrayList;
        } 
    }

    // made by shivangichotaliya 12/6/2021 15 AM
    public boolean AddTopic(Topic topic) throws RemoteException
    {
        try 
        {
            String JDBC_DRIVER ="com.mysql.jdbc.Driver";
            String DB_URL = "jdbc:mysql://localhost/PlacementTrainer_DB";
       
            Connection con = null;
            Statement stmt = null;
            String USER="root";
            String PASS="05Jan1999";

          
            Class.forName(JDBC_DRIVER);
           
            con = DriverManager.getConnection(DB_URL,USER,PASS);
        
          
            stmt=con.createStatement();
            PreparedStatement p=con.prepareStatement("insert into topicmaster(userId,LanguageId,Title,Description,LabelImage) values(?,?,?,?,?)");
		
            p.setInt(1,topic.getUserId());
			p.setInt(2,topic.getLanguageID());
            p.setString(3,topic.getTitle());
            p.setString(4,topic.getDescription());
            p.setString(5,topic.getlabelImage());

              
		
		    int i = p.executeUpdate();
		    System.out.print("\n"+i+" row inserted successfully in Topic!");

            return true;
           
        }  catch (Exception e) {
            System.out.println(e.getMessage());
            e.printStackTrace();
            return false;
            //TODO: handle exception
        } 
    }

 
    // made by shivangichotaliya 12/6/2021 1:35 AM
     public boolean EditTopic(Topic topic) throws RemoteException
    {        
        try 
        {
            String JDBC_DRIVER ="com.mysql.jdbc.Driver";
            String DB_URL = "jdbc:mysql://localhost/PlacementTrainer_DB";
       
            Connection con = null;
            Statement stmt = null;
            String USER="root";
            String PASS="05Jan1999";

          
            Class.forName(JDBC_DRIVER);
           
            con = DriverManager.getConnection(DB_URL,USER,PASS);
        
            int topicId = topic.getTopicId();
            String title = topic.getTitle();
            String description = topic.getDescription();
            String labelImage = topic.getlabelImage();

            stmt=con.createStatement();
            PreparedStatement p=con.prepareStatement("update topicmaster set Title ='"+title+"',Description='"+description+"',LabelImage='"+labelImage+"' where topicId='"+topicId+"' ");

		    int i = p.executeUpdate();
		    System.out.print("\n rows updated successfully in Topic!");

            return true;
           
        }  catch (Exception e) {
            System.out.println(e.getMessage());
            e.printStackTrace();
            return false;
            //TODO: handle exception
        } 
    }


    
    // made by shivangichotaliya 12/6/2021 1:35 AM
     public boolean DeActiveTopic(Topic topic) throws RemoteException
    {        
        try 
        {
            String JDBC_DRIVER ="com.mysql.jdbc.Driver";
            String DB_URL = "jdbc:mysql://localhost/PlacementTrainer_DB";
       
            Connection con = null;
            Statement stmt = null;
            String USER="root";
            String PASS="05Jan1999";

          
            Class.forName(JDBC_DRIVER);
           
            con = DriverManager.getConnection(DB_URL,USER,PASS);
        
            int topicId = topic.getTopicId();
        
            stmt=con.createStatement();
            PreparedStatement p=con.prepareStatement("update topicmaster set isActive=0 where topicId='"+topicId+"' ");

		    int i = p.executeUpdate();
		    System.out.print("\n rows updated successfully in Topic!");

            return true;
           
        }  catch (Exception e) {
            System.out.println(e.getMessage());
            e.printStackTrace();
            return false;
            //TODO: handle exception
        } 
    }

}