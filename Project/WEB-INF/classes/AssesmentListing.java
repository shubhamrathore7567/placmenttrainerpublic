//MAde by Namjoshi 11/06/21


import java.sql.*;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.rmi.*;
import java.util.*;
import javax.servlet.http.*;


public class AssesmentListing extends HttpServlet
{
    
    protected void doGet(HttpServletRequest req,HttpServletResponse res)throws ServletException,IOException
    {
     
        res.setContentType("text/html");
        //int topicId = 1 ;   //fetch later
        boolean isStudent=true;
        
        int topicId =Integer.parseInt(req.getQueryString()); 

        System.out.println("Query String value: "+topicId);
        

        HttpSession session = req.getSession();

        session.setAttribute("topicid", topicId);
     

        PrintWriter pw=res.getWriter();
        
        try 
        {

            RegisterInterface stub=(RegisterInterface)Naming.lookup("rmi://localhost:5000/mcaiv");
            
            ArrayList AssessmentTitle= stub.getAssesment(topicId);

            if(AssessmentTitle!=null)
            {
                
                String docType ="<!DOCTYPE HTML\">\n";
                String title = "Assesment Listing";
                
               pw.println(docType + 
                    "<HTML>\n" +
                    "<HEAD><TITLE>" + title +
                    "</TITLE>"+
                    "<script src=\"https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js\"></script>" +
                    "<script src=\"https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js\"></script>" +
                    "<script src=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js\"></script>"+
                    "</HEAD>\n" +
                    "<BODY>\n" +
                    "<div class=\"container\">" +
                    "<h2>Assesments</h2>");
                    if(!isStudent)  //for mentor
                    {
                        pw.println("<button onclick=\"location.href='addQuestions.html';\">Add Assesment</button>");
                    }
                    pw.println("<TABLE Class=\"table table-hover\"><THEAD><TR><TH scope =\"col\">No.</TH><TH scope =\"col\">Title</TH></TR><TBODY>");
                    
        
                
                String topicTitle,Description,LabelImage;

                for(int i=0;i<AssessmentTitle.size();i++)
                {
                        pw.println( 
                            "<TR><TD>"+ (i+1)+" </TD><TD>" + AssessmentTitle.get(i) + "</TD>");
                        if(isStudent)//for student
                        {
                            pw.println("<TD><button onclick=\"location.href='TakeAssesment?';\">give Assesment</button></TD>");
                        }
                        
                        pw.println("</TR><br>");
                }
                pw.println("<TBODY></TABLE></DIV></BODY></HTML>");
            
            }

           
        } 
        catch (Exception e) 
        {
            e.printStackTrace();
            System.out.println(e.getMessage() + "\t hello");
        }

    }


}




