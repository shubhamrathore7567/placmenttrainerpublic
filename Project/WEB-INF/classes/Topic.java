
// made by shivangi chotaliya 10/6/2021 21:16

import java.io.*;

public class Topic implements Serializable
{
    private int topicId;
    private int userId;
    private int languageID;
    private String title;
    private String description;
    private String labelImage;

    public Topic(int topicId)
    {
        this.topicId=topicId;   
    }
    public Topic(int topicId,int userId,int languageID,String title,String description,String labelImage)
    {
        this.topicId=topicId;
        this.userId =userId;
        this.languageID=languageID;
        this.title = title;
        this.description=description;
        this.labelImage=labelImage;
    }
    public Topic(int userId,int languageID,String title,String description,String labelImage)
    {
        this.userId =userId;
        this.languageID=languageID;
        this.title = title;
        this.description=description;
        this.labelImage=labelImage;
    }
    public void setTopicId(int topicId)
    {
        this.topicId=topicId;
    }
    public int getTopicId()
    {
        return this.topicId;
    }

    public void setUserId(int userId)
    {
        this.userId=userId;
    }
    public int getUserId()
    {
        return this.userId;
    }
    public void setLanguageID(int languageID)
    {
        this.languageID=languageID;
    }
    public int getLanguageID()
    {
        return this.languageID;
    }
    
    public void setTitle(String title)
    {
        this.title = title;
    }
    public String getTitle()
    {
        return this.title;
    }
    public void setDescription(String description)
    {
        this.description = description;
    }
    public String getDescription()
    {
        return this.description;
    }
    public void setlabelImage(String labelImage)
    {
        this.labelImage = labelImage;
    }
    public String getlabelImage()
    {
        return this.labelImage;
    }
}

