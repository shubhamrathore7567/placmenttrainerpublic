//MAde by Namjoshi 11/06/21


import java.io.*;
import java.util.*;

public class Assesment implements Serializable
{

    private int userId,topicId,langId;
    private String title;
    private double weightage,passingPercentage;

    public ArrayList<Question> que = new ArrayList<Question>();

    public Assesment(int userId,String title,int topicId,int langId,double passingPercentage,double weightage)
    {
        this.userId=userId;
        this.title=title;
        this.topicId=topicId;
        this.langId=langId;
        this.passingPercentage=passingPercentage;
        this.weightage=weightage;
    }
    public Assesment()
    {

    }

    //getters
    public int getUserID()
    {
        return this.userId;
    }
    public String getTitle()
    {
        return this.title;
    }

    public int getTopicId()
    {
        return this.topicId;
    }
    public int getLangId()
    {
        return this.langId;
    }
    public double getPassingPercentage()
    {
        return this.passingPercentage;
    }
    public double getWeightage()
    {
        return this.weightage;
    }
    /* public Question[] getQuestion()
    {
        return this.que[];
    } */

    //setters
    public void setUserID(int userId)
    {
        this.userId=userId;
    }
    public void setTitle(String title)
    {
        this.title=title;
    }

    public void setTopicId(int topicId)
    {
        this.topicId=topicId;
    }
    public void setLangId(int langId)
    {
        this.langId=langId;
    }
    public void setPassingPercentage(double passingPercentage)
    {
        this.passingPercentage=passingPercentage;
    }
    public void setWeightage(double weightage)
    {
        this.weightage=weightage;
    }
    public void setQuestion(ArrayList<Question> que)
    {
        this.que=que;
    }
}