
//MAde by Namjoshi 12/06/21


import java.sql.*;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.rmi.*;
import java.util.*;
import javax.servlet.http.*;


public class TakeAssesment extends HttpServlet
{
    
    protected void doGet(HttpServletRequest req,HttpServletResponse res)throws ServletException,IOException
    {
     
        res.setContentType("text/html");
       // int topicId = 1 ;
        int assesmentId=1;   //fetch later
        HttpSession session=req.getSession(false); 
        int topicId=(int)session.getAttribute("topicid");
        
        PrintWriter pw=res.getWriter();
        
        try 
        {

            RegisterInterface stub=(RegisterInterface)Naming.lookup("rmi://localhost:5000/mcaiv");
            
            Assesment asses= stub.takeAssesment(assesmentId);
            
            session = req.getSession();

            session.setAttribute("AssesmentObjact", asses);

            if(asses!=null)
            {
                
                String docType ="<!DOCTYPE HTML\">\n";
                String title = "Take Assesment";
                
               pw.println(docType + 
                    "<HTML>\n" +
                    "<HEAD><TITLE>" + title +
                    "</TITLE>"+
                    "<script src=\"https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js\"></script>" +
                    "<script src=\"https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js\"></script>" +
                    "<script src=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js\"></script>"+
                    "</HEAD>\n" +
                    "<BODY>\n" +
                    "<div class=\"container\">" +
                    "<h1>Take Assesment</h1>"+
                    "<h3>Title= "+asses.getTitle()+"</h3>"+
                    "<h3>Passing Percentage= "+asses.getPassingPercentage()+"</h3>"+
                    "<h3>Weightage= "+asses.getWeightage()+"</h3>"+
                    "<form action=\"AssesmentResult\" method=\"POST\">");
                
                    pw.println("<TABLE Class=\"table table-hover\"><THEAD><TR><TH scope =\"col\">No.</TH><TH scope =\"col\">Question</TH></TR><TBODY>");
                    
        
                
                //System.out.println("size= "+asses.getSize());
                for(int i=0;i<asses.que.size();i++)
                {
                    pw.println("<TR><TD>"+ (i+1)+" </TD><TD>" + asses.que.get(i).getQuestion()+ "</TD>");
                    pw.println("<TR><TD> </TD><TD><input type=\"radio\" name=\"o"+i+"\" value=\"1\"/>"+ asses.que.get(i).getOp1()+ "</TD>");
                    pw.println("<TR><TD> </TD><TD><input type=\"radio\" name=\"o"+i+"\"value=\"2\"/>" + asses.que.get(i).getOp2()+ "</TD>");
                    pw.println("<TR><TD> </TD><TD><input type=\"radio\" name=\"o"+i+"\"value=\"3\"/>" + asses.que.get(i).getOp3()+ "</TD>");
                    pw.println("<TR><TD> </TD><TD><input type=\"radio\" name=\"o"+i+"\"value=\"4\"/>" + asses.que.get(i).getOp4()+ "</TD>");
                       
                    pw.println("</TR><br>");
                }
                pw.println("<TBODY></TABLE>");
                pw.println("<input type=\"submit\" value=\"Submit Test\">");

                pw.println("</form></DIV></BODY></HTML>");
            
            }

           
        } 
        catch (Exception e) 
        {
            e.printStackTrace();
            System.out.println(e.getMessage() + "\t error!");
        }

    }


}




