//MAde by Namjoshi 11/06/21


import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.rmi.*;



public class AddQuestions extends HttpServlet
{
    protected void doPost(HttpServletRequest req,HttpServletResponse res)throws ServletException,IOException
    {
     
        int assestmentId=1,userId=1,topicId=1,langId=1;   ///ids to take from session
        res.setContentType("text/html");
        
        PrintWriter pw=res.getWriter();
        
	    String question[]=req.getParameterValues("que");
        String op1[]=req.getParameterValues("op1");
        String op2[]=req.getParameterValues("op2");
        String op3[]=req.getParameterValues("op3");
        String op4[]=req.getParameterValues("op4");
        String ans[]=req.getParameterValues("ans");
        String weightage[]=req.getParameterValues("weightage");
        double passingPercentage=Double.parseDouble(req.getParameter("passingPercentage"));
        String title=req.getParameter("Atitle");
        System.out.println("title="+title);
        
        Question[] que=new Question[question.length];
        double weight,totalWeightage=0;
        for(int i=0;i<question.length;i++)
        {
            weight=Double.parseDouble(weightage[i]);
            que[i]=new Question(question[i],op1[i],op2[i],op3[i],op4[i],Integer.parseInt(ans[i]),weight,assestmentId,userId);
            totalWeightage+=weight;
        }
        
        Assesment assestment=new Assesment(userId,title,topicId,langId,passingPercentage,totalWeightage);
        try {
            RegisterInterface stub=(RegisterInterface)Naming.lookup("rmi://localhost:5000/mcaiv");
            pw.println("<html><body style='background-color: aqua;   padding-top: 10%; text-align:center;'><h2>");

            if(stub.addQuestions(que,assestment))            
            {
                res.sendRedirect("login.html"); 
            }
            else
                pw.println("Registration failed");

            pw.println("</h2></body></html>");
           
        } catch (Exception e) {
                System.out.println(e);
            //TODO: handle exception
        }
     
    }
}

