//Made by shivangi 11/06/21 23:06

import java.sql.*;
import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.http.*;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.rmi.*;
import java.util.*;


import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.io.output.*;

public class AddTopic extends HttpServlet
{
    
   private boolean isMultipart;
   private String filePath;
   private int maxFileSize = 1024 * 1024 * 10; 
   private int maxMemSize = 1024 * 1024 * 50;
   private File file ;

   public void init(){
      // Get the file location where it would be stored.
      filePath = getServletContext().getInitParameter("file-upload"); 
   }
   
    
    protected void doPost(HttpServletRequest req,HttpServletResponse res)throws ServletException,IOException
    { 

         res.setContentType("text/html");
         HttpSession session=req.getSession(false); 
         int sessionUserid=(int)session.getAttribute("s_userid");
   
         int LanguageId = 1;

         String title =req.getParameter("title");
         String description =req.getParameter("description");
         String labelImage =req.getParameter("labelImage");

         //labelImage = "upload/images/" +labelImage ;

         Topic topic = new Topic(sessionUserid,1,title,description,labelImage);
         
         PrintWriter pw=res.getWriter();
         
         try {

               RegisterInterface stub=(RegisterInterface)Naming.lookup("rmi://localhost:5000/mcaiv");
               
               if(stub.AddTopic(topic)){
                  res.sendRedirect("MantorTopicListing"); 
               }else{
                     pw.println("Topic Addintion failed");
               }

         } catch (Exception e) {
               e.printStackTrace();
               System.out.println(e.getMessage() + "\t hello");
         }

        isMultipart = ServletFileUpload.isMultipartContent(req);
        res.setContentType("text/html");
        java.io.PrintWriter out = res.getWriter( );
   /*
        if( !isMultipart ) {
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet upload</title>");  
            out.println("</head>");
            out.println("<body>");
            out.println("<p>No file uploaded</p>"); 
            out.println("</body>");
            out.println("</html>");
            return;
        }
  */
        DiskFileItemFactory factory = new DiskFileItemFactory();
   
      // maximum size that will be stored in memory
        factory.setSizeThreshold(maxMemSize);

        // Location to save data that is larger than maxMemSize.
        factory.setRepository(new File("c:\\temp"));

        // Create a new file upload handler
        ServletFileUpload upload = new ServletFileUpload(factory);

        // maximum file size to be uploaded.
        upload.setSizeMax( maxFileSize );

       try { 
         // Parse the req to get file items.
         List fileItems = upload.parseRequest(req);
	
         // Process the uploaded file items
         Iterator i = fileItems.iterator();
/*
         out.println("<html>");
         out.println("<head>");
         out.println("<title>Servlet upload</title>");  
         out.println("</head>");
         out.println("<body>");
  */ 
         while ( i.hasNext () ) {
            FileItem fi = (FileItem)i.next();
            if ( !fi.isFormField () ) {
               // Get the uploaded file parameters
               String fieldName = fi.getFieldName();
               String fileName = fi.getName();
               String contentType = fi.getContentType();
               boolean isInMemory = fi.isInMemory();
               long sizeInBytes = fi.getSize();
            
               // Write the file
               if( fileName.lastIndexOf("\\") >= 0 ) {
                  file = new File( filePath + fileName.substring( fileName.lastIndexOf("\\"))) ;
               } else {
                  file = new File( filePath + fileName.substring(fileName.lastIndexOf("\\")+1)) ;
               }
               fi.write( file ) ;
               out.println("Uploaded Filename: " + fileName + "<br>");
            }
         }
   //      out.println("</body>");
    //     out.println("</html>");
        } catch(Exception ex) {
            System.out.println(ex);
        }
    }
}

/*

import java.io.*;
import java.util.*;
 
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
 

public class upload extends HttpServlet {
   


   public void doPost(HttpServletRequest req, HttpServletResponse res)
      throws ServletException, java.io.IOException {
   
      // Check that we have a file upload req
      isMultipart = ServletFileUpload.isMultipartContent(req);
      res.setContentType("text/html");
      java.io.PrintWriter out = res.getWriter( );
   
      if( !isMultipart ) {
         out.println("<html>");
         out.println("<head>");
         out.println("<title>Servlet upload</title>");  
         out.println("</head>");
         out.println("<body>");
         out.println("<p>No file uploaded</p>"); 
         out.println("</body>");
         out.println("</html>");
         return;
      }
  
      DiskFileItemFactory factory = new DiskFileItemFactory();
   
      // maximum size that will be stored in memory
      factory.setSizeThreshold(maxMemSize);
   
      // Location to save data that is larger than maxMemSize.
      factory.setRepository(new File("c:\\temp"));

      // Create a new file upload handler
      ServletFileUpload upload = new ServletFileUpload(factory);
   
      // maximum file size to be uploaded.
      upload.setSizeMax( maxFileSize );

      try { 
         // Parse the req to get file items.
         List fileItems = upload.parseRequest(req);
	
         // Process the uploaded file items
         Iterator i = fileItems.iterator();

         out.println("<html>");
         out.println("<head>");
         out.println("<title>Servlet upload</title>");  
         out.println("</head>");
         out.println("<body>");
   
         while ( i.hasNext () ) {
            FileItem fi = (FileItem)i.next();
            if ( !fi.isFormField () ) {
               // Get the uploaded file parameters
               String fieldName = fi.getFieldName();
               String fileName = fi.getName();
               String contentType = fi.getContentType();
               boolean isInMemory = fi.isInMemory();
               long sizeInBytes = fi.getSize();
            
               // Write the file
               if( fileName.lastIndexOf("\\") >= 0 ) {
                  file = new File( filePath + fileName.substring( fileName.lastIndexOf("\\"))) ;
               } else {
                  file = new File( filePath + fileName.substring(fileName.lastIndexOf("\\")+1)) ;
               }
               fi.write( file ) ;
               out.println("Uploaded Filename: " + fileName + "<br>");
            }
         }
         out.println("</body>");
         out.println("</html>");
         } catch(Exception ex) {
            System.out.println(ex);
         }
      }
      
    /*  public void doGet(HttpServletRequest req, HttpServletResponse res)
         throws ServletException, java.io.IOException {

         throw new ServletException("GET method used with " +
            getClass( ).getName( )+": POST method required.");
      }*/


