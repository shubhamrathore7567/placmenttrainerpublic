import java.io.*;

public class User implements Serializable
{
    private int userid;
    private String name;
    private String email;
    private String username;
    private String password;
    private String contact;
    private String skills;
    private String organization;
    private String stream;

    private String userrole;

    public User(String name,String email,String username,String password,String contact,String skills,String organization,String stream)
    {
        this.name=name;
        this.email=email;
        this.username=username;
        this.password=password;
        this.contact=contact;
        this.skills=skills;
        this.organization=organization;
        this.stream=stream;
    }
	
	public User(String name,String email,String username,String password,String contact)
    {
        this.name=name;
        this.email=email;
        this.username=username;
        this.password=password;
        this.contact=contact;
    }
	
    public void setUserId(int id)
    {
        this.userid=id;
    }
    public int getUserId()
    {
        return this.userid;
    }

    public void setUserRole(String role)
    {
        this.userrole=role;
    }
    public String getUserRole()
    {
        return this.userrole;
    }
    public String getName()
    {
        return this.name;
    }
    public String getEmail()
    {
        return this.email;
    }
    public String getUserName()
    {
        return this.username;
    }
    public String getPassword()
    {
        return this.password;
    }
    public String getContact()
    {
        return this.contact;
    }
    public String getSkills()
    {
        return this.skills;
    }
    public String getOrganization()
    {
        return this.organization;
    }
    public String getStream()
    {
        return this.stream;
    }
}

