//MAde by Namjoshi 09/06/21

import java.rmi.*;
import java.util.*;

public interface RegisterInterface extends Remote
{
    public boolean register(User user) throws RemoteException;

    public String login(String username) throws RemoteException;

    public int getUserId(String username) throws RemoteException;

    public boolean addQuestions(Question[] que,Assesment assestment) throws RemoteException;

    public ArrayList getAssesment(int topicId) throws RemoteException;

    public Assesment takeAssesment(int assesmentId) throws RemoteException;

    public ArrayList<ArrayList<String>> mantorListing(int sessionUserid) throws RemoteException;  //Made by shivangi 10/06/21  
    public boolean AddTopic(Topic topic) throws RemoteException; // Made by shivangi
    public boolean EditTopic(Topic topic) throws RemoteException; // Made by shivangi
    public boolean DeActiveTopic(Topic topic) throws RemoteException; // Made by shivangi
}