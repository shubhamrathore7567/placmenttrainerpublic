
//Made by shivangi 11/06/21 23:06

import java.sql.*;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.*;
import java.rmi.*;
import java.util.*;


public class EditTopic extends HttpServlet
{
    
    protected void doPost(HttpServletRequest req,HttpServletResponse res)throws ServletException,IOException
    { 
        res.setContentType("text/html");
         HttpSession session=req.getSession(false); 
        int sessionUserid=(int)session.getAttribute("s_userid");
   
        int LanguageId = 1;


        int topicId = Integer.parseInt(req.getParameter("editTopic_hidden1"));

        String title =req.getParameter("title");
        String description =req.getParameter("description");
        String labelImage =req.getParameter("labelImage");

        

        Topic topic = new Topic(topicId,sessionUserid,1,title,description,labelImage);
        
        PrintWriter pw=res.getWriter();
        
        try {

            RegisterInterface stub=(RegisterInterface)Naming.lookup("rmi://localhost:5000/mcaiv");
            
            if(stub.EditTopic(topic)){
                 res.sendRedirect("MantorTopicListing"); 
            }else{
                  pw.println("Topic Edit is failed");
            }

        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(e.getMessage() + "\t hello");
        }

    }
}




