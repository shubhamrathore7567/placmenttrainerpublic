//MAde by Namjoshi 09/06/21

import java.rmi.*;
import java.rmi.registry.*;

public class MyServer
{
    public static void main(String[] args)
    {
        try {
            RegisterInterface stub=new RegisterRemote();
            Naming.rebind("rmi://localhost:5000/mcaiv",stub);
        } catch (Exception e) {
            System.out.println(e);
        }
    }
}

/*
For running rmi 
1)Compile all java files
javac *.java

2)create stub and skelton objact by rmic tool
rmic AdderRemote

3)Start rmi registry in one cmd
rmiregistry 5000

4)start the server in another cmd
java MyServer

5)Start the client application in another cmd
java MyClient
*/