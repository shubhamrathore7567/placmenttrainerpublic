//made by devangi

import java.io.*;

public class resource implements Serializable
{
    private int ResourceId;
	private int TopicId;
    private String Title;
    private String Description;
    private String Link;
    private String File;
	
	public resource(int ResourceId){
		this.ResourceId=ResourceId;
	}

    public resource(int ResourceId,int TopicId,String Title,String Description,String File,String Link)
    {
		this.ResourceId=ResourceId;
		this.TopicId=TopicId;
        this.Title=Title;
        this.Description=Description;
        this.File=File;
		this.Link=Link;
       
    }
	 public resource(int TopicId,String Title,String Description,String File,String Link)
    {
		
		this.TopicId=TopicId;
        this.Title=Title;
        this.Description=Description;
        this.File=File;
		this.Link=Link;
       
    }
	 public void setResourceId(int ResourceId)
   
	{
        this.ResourceId=ResourceId;
    }
    public int getResourceId()
    {
        return this.ResourceId;
    }
    public void setTopicId(int TopicId)
    {
        this.TopicId=TopicId;
    }
    public int getTopicId()
    {
        return this.TopicId;
    }

    public void settitle(String title)
    {
        this.Title=title;
    }
    public String gettitle()
    {
        return this.Title;
    }
	public void setdescription(String description)
    {
        this.Description=description;
    }
    public String getdescription()
    {
        return this.Description;
    }
	
	public void setfile(String file)
    {
        this.File=file;
    }
	public String getfile()
    {
        return this.File;
    }
	public void setlink(String link)
    {
        this.Link=link;
    }
    public String getlink()
    {
        return this.Link;
    }
    
}
