
//Made by shivangi 10/06/21 1 Am

import java.sql.*;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.*;
import java.rmi.*;
import java.util.*;


public class MantorTopicListing extends HttpServlet
{
    
    protected void doGet(HttpServletRequest req,HttpServletResponse res)throws ServletException,IOException
    {
     
        res.setContentType("text/html");
        HttpSession session=req.getSession(false); 
        int sessionUserid=(int)session.getAttribute("s_userid");
   
        PrintWriter pw=res.getWriter();
        
        try {

            RegisterInterface stub=(RegisterInterface)Naming.lookup("rmi://localhost:5000/mcaiv");
            
            ArrayList<ArrayList<String>> arrayList= stub.mantorListing(sessionUserid);


                String docType ="<!DOCTYPE HTML\">\n";
                String title = "Message page @2";
                
               pw.println(docType + 
                    "<HTML>\n" +
                    "<HEAD>" + req.getContextPath() +
                    "<meta charset=\"utf-8\">" +
                    "<meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">" +
                    "<link rel=\"stylesheet\" href=\"https://fonts.googleapis.com/css?family=Roboto|Varela+Round\">" +
                    "<link rel=\"stylesheet\" href=\"https://fonts.googleapis.com/icon?family=Material+Icons\">"+
                    "<link rel=\"stylesheet\" href=\"https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css\">" +
                    "<link rel=\"stylesheet\" href=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css\">" +
                    "<link rel=\"stylesheet\" href=\"  "+ req.getContextPath() +"/css/StyleMantorListing.css\">" +
//out.println("<link rel='stylesheet' type='text/css' href='" + req.getContextPath() +  "/css/EmpSearchServlet.css' />");
                    //"<script src=\""+ req.getContextPath() +"/css/font-awesome.min.css\"></script>" +
//                        "<script src=\""+ req.getContextPath() +"/vendor/jquery/jquery.min.js\"></script>"+
                    "<script src=\"https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js\"></script>"+
                    "<script src=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js\"></script>"+
                    "<script src=\""+ req.getContextPath() +"/js/main.js\"></script>"+
                //"<script src=\""+ req.getContextPath() +"/js/bootstrap/bootstrap.min.js\"></script>"+
                "</HEAD>\n" +
                    "<body>" +
                        "<div class=\"container\">" +
                            "<div class=\"table-responsive\">" +
                                "<div class=\"table-wrapper\">" +
                                    "<div class=\"table-title\">" +
                                        "<div class=\"row\">" +
                                            "<div class=\"col-xs-6\">" +
                                                "<h2><b>Topic Listing</b></h2>" +
                                            "</div>" +
                                            "<div class=\"col-xs-6\">" +
                                                "<a href=\"#addTopicModal\" class=\"btn btn-success\" data-toggle=\"modal\"><i class=\"material-icons\">&#xE147;</i> <span>Add New Topic</span></a>"+    
                                            "</div>" +
                                        "</div>" +
                                    "</div>" +
                                    "<table class=\"table table-striped table-hover\">" +
                                        "<thead>" +
                                            "<tr>" +
                                                "<th>Topic</th>"+
                                                "<th>Description</th>" +
                                                "<th>LabelImage</th>" +
                                                "<th>Edit</th>" +
                                                "<th>Deactivate</th>" +
                                                "<th>Add Resources </th>" +
                                                "<th>View Assessment</th>" +
                                            "</tr>" +
                                        "</thead>" +
                                        "<tbody>");
                                      
                                            String topicId,languageId;
                                            String topicTitle,description,labelImage;

                                            for(int i=0;i<arrayList.size();i++){
                                                    topicId = arrayList.get(i).get(0);
                                                    
                                                    languageId = arrayList.get(i).get(1);                    
                                                    topicTitle = arrayList.get(i).get(2);
                                                    description = arrayList.get(i).get(3);
                                                    labelImage = arrayList.get(i).get(4);
                                        


                                        
//<input type=\"hidden\" name=\"topicId\" id=\"topicId\"  value=\" "+ topicId +"\"  class=\"EditTopicClass\" \"/>
                                            pw.println(                         
                                                "<tr>"+
                                                    "<td>" + topicTitle+ "</td>"+
                                                    "<td>"+description +"</td>" +                
                                                    "<td><img src =\""+ labelImage+"\" class=\"img-rounded\"  width=\"304\" height=\"236\"></td>"+
                                                    "<td><a  onclick=\"editTopicModal("+topicId+")\"  class=\"edit\" data-toggle=\"modal\"><i class=\"material-icons\" data-toggle=\"tooltip\" title=\"Edit\">&#xE254;</i></a></td>"+
                                                    "<td><input type=\"button\" class=\"mybutton btn btn-primary\" onclick=\"showDeactivateModal("+topicId+")\" value=\"Deactivate\"></td>"+
                                                    "<td><input type=\"button\" class=\"mybutton btn btn-primary\"  value=\"Add Resources\"></td>"+
                                                    "<td><input type=\"button\" class=\"mybutton btn btn-primary\" onclick=\"location.href='AssesmentListing?"+topicId+"';\" value=\"View Assessment\"></td>"+
                                                                       
                                                "</tr>");                                                
                                            }
                                            // href=\"#editTopicModal\"
                                            pw.println(                                                        
                                        "</tbody>"+
                                    "</table>"+
                                "</div>" +
                            "</div>"+        
                        "</div>"+
                        //<!-- Add Modal HTML -->
                        "<div id=\"addTopicModal\" class=\"modal fade\">"+
                            "<div class=\"modal-dialog\">"+
                                "<div class=\"modal-content\">" +
                                    "<form method=\"post\" action=\"AddTopic\" >"+
                                        "<div class=\"modal-header\">" +						
                                            "<h4 class=\"modal-title\">Add Topic</h4>"+
                                            "<button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-hidden=\"true\">&times;</button>"+
                                        "</div>"+
                                        "<div class=\"modal-body\">"+					
                                            "<div class=\"form-group\">"+
                                                "<label>Title</label>"+
                                                "<input type=\"text\" name=\"title\" class=\"form-control\" required>"+
                                            "</div>"+
                                            "<div class=\"form-group\">"+
                                                "<label>Description</label>"+
                                                "<textarea class=\"form-control\" name=\"description\" required></textarea>"+
                                            "</div>"+
                                            "<div class=\"form-group\">"+   
                                                "<label>LabeImage</label>"+
                                                //"<input type=\"file\"  name=\" "+ req.getContextPath() +"upload/images/labelImage\" id=\"labelImage\">"+
                                                "<input type=\"file\"  name=\"file\" id=\"labelImage\" accept=\"image/png, image/jpeg\">"+
                                            "</div>"+                    					
                                        "</div>"+
                                        "<div class=\"modal-footer\">"+
                                            "<input type=\"button\" class=\"btn btn-default\" data-dismiss=\"modal\" value=\"Cancel\">"+
                                            "<input type=\"submit\" class=\"btn btn-success\" value=\"Add\">"+
                                        "</div>"+
                                    "</form>"+
                                "</div>"+
                            "</div>"+
                        "</div>"+
                        //<!-- Edit Modal HTML -->
                          "<div id=\"editTopicModal\" class=\"modal fade\">"+
                            "<div class=\"modal-dialog\">"+
                                "<div class=\"modal-content\">" +
                                    "<form method=\"post\" action=\"EditTopic\">"+
                                        "<div class=\"modal-header\">" +						
                                            "<h4 class=\"modal-title\">Edit Topic</h4>"+
                                            "<button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-hidden=\"true\">&times;</button>"+
                                        "</div>"+
                                        "<div class=\"modal-body\">"+					
                                            "<div class=\"form-group\">"+
                                                "<label>Title</label>"+
                                                "<input id=\"editTopic_hidden1\" type=\"hidden\" name=\"editTopic_hidden1\"  class=\"form-control\" >"+
                                                "<input type=\"text\" name=\"title\" class=\"form-control\" required>"+
                                            "</div>"+
                                            "<div class=\"form-group\">"+
                                                "<label>Description</label>"+
                                                "<textarea class=\"form-control\" name=\"description\" required></textarea>"+
                                            "</div>"+
                                            "<div class=\"form-group\">"+   
                                                "<label>LabeImage</label>"+
                                                //"<input type=\"file\"  name=\" "+ req.getContextPath() +"upload/images/labelImage\" id=\"labelImage\">"+
                                                "<input type=\"file\"  name=\"labelImage\" id=\"labelImage\" accept=\"image/png, image/jpeg\">"+
                                            "</div>"+                    					
                                        "</div>"+
                                        "<div class=\"modal-footer\">"+
                                            "<input type=\"button\" class=\"btn btn-default\" data-dismiss=\"modal\" value=\"Cancel\">"+
                                            "<input type=\"submit\" class=\"btn btn-info\" value=\"Save\">"+
                                        "</div>"+
                                    "</form>"+
                                "</div>"+
                            "</div>"+
                        "</div>"+
                        //<!-- Delete Modal HTML -->
                        "<div id=\"deactivateTopicModal\" class=\"modal fade\">" +
                            "<div class=\"modal-dialog\">"+
                                "<div class=\"modal-content\">"+
                                     "<form method=\"post\" action=\"DeactivateTopic\">"+
                                        "<div class=\"modal-header\">"+						
                                            "<h4 class=\"modal-title\">DeActive Topic</h4>"+
                                            "<button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-hidden=\"true\">&times;</button>"+
                                        "</div>"+
                                        "<div class=\"modal-body\">"+	
                                             "<input id=\"deactivate_hidden1\" type=\"hidden\" name=\"deactivate_topicId\" class=\"form-control\" >"+	
                                            "<p>Are you sure you want to DeActive these Topic?</p>"+
                                            "<p class=\"text-warning\"><small>This action cannot be undone.</small></p>"+
                                        "</div>"+
                                        "<div class=\"modal-footer\">"+
                                            "<input type=\"button\" class=\"btn btn-default\" data-dismiss=\"modal\" value=\"Cancel\">"+
                                            "<input type=\"submit\" class=\"btn btn-danger\" value=\"Deactive\">"+
                                        "</div>"+
                                    "</form>"+
                                "</div>"+
                            "</div>"+
                        "</div>"+
                       
                    "</body>"+
                "</html>");




/*                String topicTitle,Description,LabelImage;

                for(int i=0;i<arrayList.size();i++){
                        topicTitle = arrayList.get(i).get(0);
                        Description = arrayList.get(i).get(1);
                        LabelImage = arrayList.get(i).get(2);
                
                        pw.println( 
                            "<TR><TD>"+ topicTitle+" </TD><TD>" + Description + "</TD><TD><img src =\""+ LabelImage+"\" class=\"img-rounded\"  width=\"304\" height=\"236\"></TD><TD><input type=\"submit\" value=\"Add Resources\" name=\"conf\"/></TD></TR>\n");
                }
                pw.println("<TBODY></TABLE></DIV></BODY></HTML>");
  */          

           
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(e.getMessage() + "\t hello moantor");
        }

    }
}




