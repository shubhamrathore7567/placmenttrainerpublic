//MAde by Namjoshi 09/06/21
import java.rmi.*;
import java.rmi.server.*;
import java.sql.*;

public class RegisterRemote extends UnicastRemoteObject implements RegisterInterface{

    RegisterRemote() throws RemoteException{
        super();
    } 

    public boolean register(String name,String email,String username,String password,String contact,String skills,String organization,String stream) throws RemoteException
    {
         //for jdbc 
        
        boolean isStudent=false;
        // String query="";
       
        try 
        {
            String JDBC_DRIVER ="com.mysql.jdbc.Driver";
            String DB_URL = "jdbc:mysql://localhost/placementtrainer_db";
       
            Connection con = null;
            Statement stmt = null;
            String USER="root";
            String PASS="";

          
            Class.forName(JDBC_DRIVER);
           
            con = DriverManager.getConnection(DB_URL,USER,PASS);
        
          
            stmt=con.createStatement();
            
           
          //  query="insert into user values (\""+uid+"\",\""+pass+"\")";
         

            PreparedStatement p=con.prepareStatement("insert into usermaster(username,name,password,email,contact,userRole) values(?,?,?,?,?,?)");
		

			p.setString(1,username);
	        p.setString(2,name);
            p.setString(3,password);
            p.setString(4,email);
            p.setString(5,contact);

            if(skills.equals(""))
            {
                isStudent=true;
                p.setString(6,"Student");
            }   
            else
                p.setString(6,"Mentor");
		
		    int i = p.executeUpdate();
		    System.out.print("\n"+i+" row inserted successfully!");

            
            stmt=con.createStatement();      
            String query="Select userId from usermaster where username=\""+username+"\"";
            ResultSet rs = stmt.executeQuery(query);

            rs.next();
            String uid=rs.getString("userId");


            if(isStudent)
            {
                p=con.prepareStatement("insert into studentprofile(userid,organization,stream) values(?,?,?)");
                p.setString(1,uid);
	            p.setString(2,organization);
                p.setString(3,stream);
            }
            else
            {
                p=con.prepareStatement("insert into mentorprofile(userid,skills) values(?,?)");
                p.setString(1,uid);
	            p.setString(2,skills);
            }
            int i2 = p.executeUpdate();
            
            return true;
           
        }  catch (Exception e) {
            System.out.println(e.getMessage());
            e.printStackTrace();
            return false;
            //TODO: handle exception
        } 
    }
}