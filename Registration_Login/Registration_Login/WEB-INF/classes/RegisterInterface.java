//MAde by Namjoshi 09/06/21
import java.rmi.*;

public interface RegisterInterface extends Remote
{
    public boolean register(String name,String email,String username,String password,String contact,String skills,String organization,String stream) throws RemoteException;
}