
//Made by shivangi 11/06/21 23:06

import java.sql.*;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.rmi.*;
import java.util.*;
import java.io.File;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.io.output.*;



public class editresource extends HttpServlet
{
    private boolean isMultipart;
    private String filePath;
    private int maxFileSize = 1024 * 1024 * 10; 
    private int maxMemSize = 1024 * 1024 * 50;
    private File file ;
 
    public void init( ){
       // Get the file location where it would be stored.
       filePath = getServletContext().getInitParameter("file-upload"); 
    }
    
    protected void doPost(HttpServletRequest req,HttpServletResponse res)throws ServletException,IOException
    { 
        res.setContentType("text/html");
        //int sessionUserid = 7 ;

        //int LanguageId = 1;

        System.out.println("Edit in ");
        int resourceid =Integer.parseInt(req.getParameter("editResource_hidden1"));
        System.out.println("Edit in " + resourceid);
    
        String title =req.getParameter("title");
        String description =req.getParameter("description");
        String file1 =req.getParameter("file");
        String link =req.getParameter("link");
      
        resource resource = new resource(resourceid,title,description,file1,link);
        
        PrintWriter pw=res.getWriter();
        
        try {

            RegisterInterface stub=(RegisterInterface)Naming.lookup("rmi://localhost:5000/mcaiv");
            
            if(stub.editresource(resource)){
                 res.sendRedirect("ViewResources"); 
            }else{
                  pw.println("Topic Edit is failed");
            }

        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(e.getMessage() + "\t hello");
        }
        isMultipart = ServletFileUpload.isMultipartContent(req);
        res.setContentType("text/html");
        java.io.PrintWriter out = res.getWriter( );
     
        if( !isMultipart ) {
           out.println("<html>");
           out.println("<head>");
           out.println("<title>Servlet upload</title>");  
           out.println("</head>");
           out.println("<body>");
           out.println("<p>No file uploaded</p>"); 
           out.println("</body>");
           out.println("</html>");
           return;
        }
    
        DiskFileItemFactory factory = new DiskFileItemFactory();
     
        // maximum size that will be stored in memory
        factory.setSizeThreshold(maxMemSize);
     
        // Location to save data that is larger than maxMemSize.
        factory.setRepository(new File("c:\\temp"));
  
        // Create a new file upload handler
        ServletFileUpload upload = new ServletFileUpload(factory);
     
        // maximum file size to be uploaded.
        upload.setSizeMax( maxFileSize );
  
        try { 
           // Parse the request to get file items.
           List fileItems = upload.parseRequest(req);
      
           // Process the uploaded file items
           Iterator i = fileItems.iterator();
  
           out.println("<html>");
           out.println("<head>");
           out.println("<title>Servlet upload</title>");  
           out.println("</head>");
           out.println("<body>");
     
           while ( i.hasNext () ) {
              FileItem fi = (FileItem)i.next();
              if ( !fi.isFormField () ) {
                 // Get the uploaded file parameters
                 String fieldName = fi.getFieldName();
                 String fileName = fi.getName();
                 String contentType = fi.getContentType();
                 boolean isInMemory = fi.isInMemory();
                 long sizeInBytes = fi.getSize();
              
                 // Write the file
                 if( fileName.lastIndexOf("\\") >= 0 ) {
                    file = new File( filePath + fileName.substring( fileName.lastIndexOf("\\"))) ;
                 } else {
                    file = new File( filePath + fileName.substring(fileName.lastIndexOf("\\")+1)) ;
                 }
                 fi.write( file ) ;
                 out.println("Uploaded Filename: " + fileName + "<br>");
              }
           }
           out.println("</body>");
           out.println("</html>");
           } catch(Exception ex) {
              System.out.println(ex);
           }

    }
}




