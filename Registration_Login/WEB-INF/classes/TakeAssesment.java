
//MAde by Namjoshi 12/06/21


import java.sql.*;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.rmi.*;
import java.util.*;
import javax.servlet.http.*;


public class TakeAssesment extends HttpServlet
{
    
    protected void doGet(HttpServletRequest req,HttpServletResponse res)throws ServletException,IOException
    {
     
        
        res.setContentType("text/html");
       // int topicId = 1 ;
       int assesmentId =Integer.parseInt(req.getQueryString());
        HttpSession session=req.getSession(false); 
        int topicId=(int)session.getAttribute("topicid");
        session.setAttribute("assesmentid", assesmentId);
        PrintWriter pw=res.getWriter();

        String html = "<!DOCTYPE html>"+
        "<html lang='en'>"+
        "<head>"+
        "<meta charset='UTF-8'>"+
        "<meta http-equiv='X-UA-Compatible' content='IE=edge'>"+
        "<meta name='viewport' content='width=device-width, initial-scale=1.0'>"+
        "<title>Portfolio</title>"+
        "<link rel='stylesheet' href='https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback'>"+
        "<link rel='stylesheet' href='./dist/css/adminlte.css'>"+
        "<link rel='stylesheet' href='./plugins/fontawesome-free/css/all.css'>"+
        "<link rel='stylesheet' href='./dist/css/custom.css'>"+
        "</head>"+
        "<body class='hold-transition layout-top-nav'>"+
        "<div class='wrapper'>"+
        "<nav class='main-header navbar navbar-expand-md navbar-light navbar-white' style='background-color:#125d98;'>"+
        "<a href='../../index3.html' class='navbar-brand'>"+
          
          "<span class='brand-text font-weight-light'>Online Prep</span>"+
        "</a>"+
  
        "<button class='navbar-toggler order-1' type='button' data-toggle='collapse' data-target='#navbarCollapse' aria-controls='navbarCollapse' aria-expanded='false' aria-label='Toggle navigation'>"+
          "<span class='navbar-toggler-icon'></span>"+
        "</button>"+
  
        "<div class='collapse navbar-collapse order-3' id='navbarCollapse'>"+
          "<ul class='navbar-nav ml-auto'>"+
            "<li class='nav-item'>"+
              "<a href='index.html' class='nav-link'>Home</a>"+
            "</li>"+
            "<li class='nav-item'>"+
              "<a href='Topics.html' class='nav-link'>Topics</a>"+
            "</li>"+
            "<li class='nav-item'>"+
              "<a href='Mentors.html' class='nav-link'>Mentors</a>"+
            "</li>"+
            "<li class='nav-item'>"+
              "<a href='Friends.html' class='nav-link'>Friends</a>"+
            "</li>"+
            "<li class='nav-item'>"+
              "<a href='Contact-us.html' class='nav-link'>Contact us</a>"+
            "</li>"+
            "<li class='nav-item'>"+
              "<a href='#' class='nav-link'>Contact</a>"+
            "</li>"+
            "<li class='nav-item dropdown'>"+
              "<a id='dropdownSubMenu1' href='#' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false' class='nav-link dropdown-toggle'>"+
                "<i class='fas fa-user mr-2'></i>"+
              "</a>"+
              "<ul aria-labelledby='dropdownSubMenu1' class='dropdown-menu border-0 shadow'>"+
                "<li><a href='Profile.html' class='dropdown-item'>Profile </a></li>"+
                "<li><a href='change-pswd.html' class='dropdown-item'>Change Password</a></li>"+
  
                "<li class='dropdown-divider'></li>"+
                "<li><a href='Profile.html' class='dropdown-item'>Logout</a></li>"+
              "</ul>"+
            "</li>"+
          "</ul>"+
        "</div>"+
    "</nav>"+
"</div>"+
"<div class='content-wrapper pt-2'>"+
    "<div class='container'>";


    String footer="</div>"+
    "</div>"+
    " <footer class='main-footer' style='background-color:black;'>"+
        "<center>"+
          "<strong>Copyright &copy; 2014-2021 <a href='#'>OPR</a>.</strong> All rights reserved."+
        "</center>"+
      "</footer>"+
    "<script src='./plugins/jquery/jquery.min.js'></script>"+
    "<script src='./plugins/bootstrap/js/bootstrap.bundle.js'></script>"+
    "<script src='./dist/js/adminlte.js'></script>"+
    "<script src='./dist/js/demo.js'></script>"+
"</body>"+
"</html>";



        
        try 
        {

            RegisterInterface stub=(RegisterInterface)Naming.lookup("rmi://localhost:5000/mcaiv");
            
            Assesment asses= stub.takeAssesment(assesmentId);
            
            session = req.getSession();

            session.setAttribute("AssesmentObjact", asses);

            if(asses!=null)
            {
                
                String docType ="<!DOCTYPE HTML\">\n";
                String title = "Take Assesment";
                
               pw.println(html + 
                    "<h1>Give Assesment</h1><br><br>"+
                    "<b>Assessment Title= "+asses.getTitle()+"</b>"+
                    "<br><b>Weightage= "+asses.getWeightage()+"</b>"+
                    "<br><b>Passing Percentage= "+asses.getPassingPercentage()+" %</b><br>"+
                    "<form action=\"AssesmentResult\" method=\"POST\">");
                
                    pw.println("<TABLE Class=\"table table-hover\"><THEAD><TR><TH scope =\"col\">No.</TH><TH scope =\"col\">Question</TH></TR><TBODY>");
                    
        
                
                //System.out.println("size= "+asses.getSize());
                for(int i=0;i<asses.que.size();i++)
                {
                    pw.println("<TR><TD>"+ (i+1)+" </TD><TD>" + asses.que.get(i).getQuestion()+ "</TD>");
                    pw.println("<TR><TD> </TD><TD><input type=\"radio\" name=\"o"+i+"\" value=\"1\"/>"+ asses.que.get(i).getOp1()+ "</TD>");
                    pw.println("<TR><TD> </TD><TD><input type=\"radio\" name=\"o"+i+"\"value=\"2\"/>" + asses.que.get(i).getOp2()+ "</TD>");
                    pw.println("<TR><TD> </TD><TD><input type=\"radio\" name=\"o"+i+"\"value=\"3\"/>" + asses.que.get(i).getOp3()+ "</TD>");
                    pw.println("<TR><TD> </TD><TD><input type=\"radio\" name=\"o"+i+"\"value=\"4\"/>" + asses.que.get(i).getOp4()+ "</TD>");
                       
                    pw.println("</TR><br>");
                }
                pw.println("<TBODY></TABLE>");
                pw.println("<input type=\"submit\" value=\"Submit Test\">");

                pw.println("</form>"+footer);
            
            }

           
        } 
        catch (Exception e) 
        {
            e.printStackTrace();
            System.out.println(e.getMessage() + "\t error!");
        }

    }


}




