//shubham 2021-06-13 added

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.rmi.*;
import java.util.ArrayList;

public class ViewPortfolio extends HttpServlet {
    public void doGet(HttpServletRequest req,HttpServletResponse res)throws ServletException,IOException
    {
        try{
            res.setContentType("text/html");
            PrintWriter pw=res.getWriter();
            PortfolioView portfolio = new PortfolioView();
            RegisterInterface stub=(RegisterInterface)Naming.lookup("rmi://localhost:5000/mcaiv");
            portfolio = stub.GetPortfolio(1);
            if(portfolio == null)
            {
                res.sendRedirect("AddPortfolio.html");
            }
            String html = "<!DOCTYPE html>"+
            "<html lang='en'>"+
            "<head>"+
            "<meta charset='UTF-8'>"+
            "<meta http-equiv='X-UA-Compatible' content='IE=edge'>"+
            "<meta name='viewport' content='width=device-width, initial-scale=1.0'>"+
            "<title>Portfolio</title>"+
            "<link rel='stylesheet' href='https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback'>"+
            "<link rel='stylesheet' href='./dist/css/adminlte.css'>"+
            "<link rel='stylesheet' href='./plugins/fontawesome-free/css/all.css'>"+
            "<link rel='stylesheet' href='./dist/css/custom.css'>"+
            "</head>"+
            "<body class='hold-transition layout-top-nav'>"+
            "<div class='wrapper'>"+
            "<nav class='main-header navbar navbar-expand-md navbar-light navbar-white'>"+
            "<a href='../../index3.html' class='navbar-brand'>"+
              
              "<span class='brand-text font-weight-light'>Online Prep</span>"+
            "</a>"+
      
            "<button class='navbar-toggler order-1' type='button' data-toggle='collapse' data-target='#navbarCollapse' aria-controls='navbarCollapse' aria-expanded='false' aria-label='Toggle navigation'>"+
              "<span class='navbar-toggler-icon'></span>"+
            "</button>"+
      
            "<div class='collapse navbar-collapse order-3' id='navbarCollapse'>"+
              "<ul class='navbar-nav ml-auto'>"+
                "<li class='nav-item'>"+
                  "<a href='#home' class='nav-link'>Home</a>"+
                "</li>"+
                "<li class='nav-item'>"+
                  "<a href='#topics' class='nav-link'>Topics</a>"+
                "</li>"+
                "<li class='nav-item'>"+
                  "<a href='#mentors' class='nav-link'>Mentors</a>"+
                "</li>"+
                "<li class='nav-item'>"+
                  "<a href='#contact' class='nav-link'>ContactUs</a>"+
                "</li>"+
                "<li class='nav-item'>"+
                  "<a href='#' class='nav-link'>Contact</a>"+
                "</li>"+
                "<li class='nav-item dropdown'>"+
                  "<a id='dropdownSubMenu1' href='#' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false' class='nav-link dropdown-toggle'>"+
                    "<i class='fas fa-user mr-2'></i>"+
                  "</a>"+
                  "<ul aria-labelledby='dropdownSubMenu1' class='dropdown-menu border-0 shadow'>"+
                    "<li><a href='#' class='dropdown-item'>Profile </a></li>"+
                    "<li><a href='#' class='dropdown-item'>Change Password</a></li>"+
      
                    "<li class='dropdown-divider'></li>"+
                    "<li><a href='#' class='dropdown-item'>Logout</a></li>"+
                  "</ul>"+
                "</li>"+
              "</ul>"+
            "</div>"+
        "</nav>"+
    "</div>"+
    "<div class='content-wrapper pt-2'>"+
        "<div class='container'>";

        String personalDetail = "<div class='card'>"+            
                "<div class='card-body'>"+
                            "<div class='description'>"+
                                "Name : " + portfolio.FullName+ " <br>"+
                                "Email : "+ portfolio.Email +" <br>"+
                                "Contact : "+ portfolio.Contact +" <br>"+
                                "Age : "+ portfolio.Age +" <br>"+
                                "Profession : "+ portfolio.Profession +" <br>"+
                                "Highest Education: " + portfolio.HighestEducation +
                            "</div>"+
                "</div>"+
            "</div>";
            String Education = "<div class='card'>"+
                "<div class='card-header'>"+
                    "<h2>Education Detail</h2>"+
                "</div>"+
                "<div class='card-body'>"+
                    "<table class='table table-head-fixed'>"+
                        "<thead>"+
                          "<tr>"+
                            "<th>Serial no</th>"+
                            "<th>SEM/Class</th>"+
                            "<th>University/Board</th>"+
                            "<th>Passing year</th>"+
                            "<th>Passing Percantage</th>"+
                          "</tr>"+
                        "</thead>"+
                        "<tbody>";
                        int serialNo = 0;
                        for(EducationDetail educationDetail : portfolio.EducationDetailList)
                        {
                            serialNo++;
                            Education += "<tr><td>"+serialNo+"</td><td>"+educationDetail.Semister + "</td><td>"+educationDetail.Degree+
                            "</td><td>"+educationDetail.PassingYear+"</td><td>"+educationDetail.Percentage+"</td></tr>";
                        }
                        Education += "</tbody></table></div></div>";
                        String Project = "<div class='card'>"+
                        "<div class='card-header'>"+
                            "<h2>Project Detail</h2>"+
                        "</div>"+
                        "<div class='card-body'>"+
                            "<h3><strong>Project Title</strong> : "+ portfolio.ProjectTitle +"</h3>"+
                            "<h4><strong>Project Description:</strong></h4>"+
                            "<p>"+ portfolio.ProjectDescription+"</p>"+
                            "<h4><strong>Project Detail:</strong></h4>"+
                            "<table class='table'>"+
                                "<thead>"+
                                    "<th>Title</th>"+
                                    "<th>Description</th>"+
                                "</thead>"+
                                "<tbody>"+
                                    "<tr>"+
                                        "<td>Frontend Technology</td>"+
                                        "<td>"+ portfolio.FrontendTech +"</td>"+
                                    "</tr>"+
                                    "<tr>"+
                                        "<td>Backend Technology</td>"+
                                        "<td>"+ portfolio.BackendTech +"</td>"+
                                    "</tr>"+
                                    "<tr>"+
                                        "<td>DBMS</td>"+
                                        "<td>"+portfolio.Dbms+"</td>"+
                                    "</tr>"+
                                "</tbody>"+
                            "</table>"+
                        "</div>"+
                    "</div>"+
                "</div>"+
            "</div>"+
            "<footer class='main-footer text-center'>"+
                "<div class='float-right d-none d-sm-inline'>"+
                  "Anything you want"+
                "</div>"+
                "<strong>Copyright &copy; 2014-2021 <a href='https://adminlte.io'>AdminLTE.io</a>.</strong> All rights reserved."+
              "</footer>"+
            "<script src='./plugins/jquery/jquery.min.js'></script>"+
            "<script src='./plugins/bootstrap/js/bootstrap.bundle.js'></script>"+
            "<script src='./dist/js/adminlte.js'></script>"+
            "<script src='./dist/js/demo.js'></script>"+
        "</body>"+
        "</html>";
            pw.println(html + personalDetail+ Education + Project);
            pw.close();
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
        }
    }
}
