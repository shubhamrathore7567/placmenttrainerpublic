//MAde by Namjoshi 11/06/21


import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.rmi.*;
import javax.servlet.http.*;




public class AddQuestions extends HttpServlet
{
    protected void doPost(HttpServletRequest req,HttpServletResponse res)throws ServletException,IOException
    {
     
        int langId=1;   ///ids to take from session

        HttpSession session=req.getSession(false); 
        int topicId=(int)session.getAttribute("topicid");
        int userId=(int)session.getAttribute("s_userid");
        res.setContentType("text/html");
        System.out.println("Topicid= "+topicId+"\t USerId="+userId);
        PrintWriter pw=res.getWriter();
        
	    String question[]=req.getParameterValues("que");
        String op1[]=req.getParameterValues("op1");
        String op2[]=req.getParameterValues("op2");
        String op3[]=req.getParameterValues("op3");
        String op4[]=req.getParameterValues("op4");
        String ans[]=req.getParameterValues("ans");
        String weightage[]=req.getParameterValues("weightage");
        double passingPercentage=Double.parseDouble(req.getParameter("passingPercentage"));
        String title=req.getParameter("Atitle");
        System.out.println("title="+title);
        
        Question[] que=new Question[question.length];
        double weight,totalWeightage=0;
        for(int i=0;i<question.length;i++)
        {
            weight=Double.parseDouble(weightage[i]);
            que[i]=new Question(question[i],op1[i],op2[i],op3[i],op4[i],Integer.parseInt(ans[i]),weight,userId);
            totalWeightage+=weight;
        }
        
        Assesment assestment=new Assesment(userId,title,topicId,langId,passingPercentage,totalWeightage);
        try {
            RegisterInterface stub=(RegisterInterface)Naming.lookup("rmi://localhost:5000/mcaiv");
       
            
            int assesmentId=stub.addQuestions(que,assestment);
            System.out.println("assesID= "+assesmentId);
            
            session = req.getSession();

            session.setAttribute("assesmentid", assesmentId);           
           
            res.sendRedirect("AssesmentListing?"+session.getAttribute("topicid")); 
              
           
        } catch (Exception e) {
                System.out.println(e);
            //TODO: handle exception
        }
     
    }
}

