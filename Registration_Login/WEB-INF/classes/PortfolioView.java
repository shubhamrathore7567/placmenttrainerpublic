import java.util.*;
import java.io.*;

public class PortfolioView implements Serializable{
    public int ProfileID;
    public int UserId;
    public String ProfilePic;
    public String HighestEducation;
    public String FullName;
    public String Email;
    public String Contact;
    public String Profession;
    public String Age;
    public ArrayList<EducationDetail> EducationDetailList;
    public String ProjectTitle;
    public String ProjectDescription;
    public String FrontendTech;
    public String BackendTech;
    public String Dbms;
    public String BaseLanguage;
}
