//Made by shivangi 1/06/21  17:20

import java.sql.*;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.*;
import java.rmi.*;
import java.util.*;


public class DeactivateTopic extends HttpServlet
{
    
    protected void doPost(HttpServletRequest req,HttpServletResponse res)throws ServletException,IOException
    { 
        res.setContentType("text/html");
         HttpSession session=req.getSession(false); 
        int sessionUserid=(int)session.getAttribute("s_userid");
        
        int topicId = Integer.parseInt(req.getParameter("deactivate_topicId"));
        System.out.println("topic id in page is :" + topicId);
        Topic topic = new Topic(topicId);
        
        PrintWriter pw=res.getWriter();
        
        try {

            RegisterInterface stub=(RegisterInterface)Naming.lookup("rmi://localhost:5000/mcaiv");
            
            if(stub.DeActiveTopic(topic)){
                 res.sendRedirect("MantorTopicListing"); 
            }else{
                  pw.println("Topic Deactivaton failed");
            }

        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(e.getMessage() + "\t hello--");
        }

    }
}




