//MAde by Namjoshi 09/06/21


import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.rmi.*;

import java.sql.*;
import javax.servlet.http.*;

public class Login extends HttpServlet
{
    protected void doPost(HttpServletRequest req,HttpServletResponse res)throws ServletException,IOException
    {
     
        res.setContentType("text/html");
        String username=req.getParameter("name");
        String password=req.getParameter("password");
        String uPassword;

        PrintWriter pw=res.getWriter();
     
        try 
        {
            RegisterInterface stub=(RegisterInterface)Naming.lookup("rmi://localhost:5000/mcaiv");

            uPassword=stub.login(username);            
            
        

            if(password.equals(uPassword))
            {
                
                int userId=stub.getUserId(username);
                
                HttpSession session = req.getSession();

                session.setAttribute("s_username", username);
                session.setAttribute("s_userid", userId);

                String userRole=stub.getUserRole(username);
                session.setAttribute("userrole", userRole);
                
                res.sendRedirect("dashboard/pages/layout/index.html");    //redirect home page later

            }     
            else
            {
                pw.println("<html><body>");
                pw.println("<script> window.alert(\"Invalid Username or password!!!\");window.location.href='login.html';");
                pw.println("</script> </body></html>"); 
                       
            } 
        }      
        catch (Exception e) 
        {
            System.out.println(e);
    
        }

        pw.close();
    }
}