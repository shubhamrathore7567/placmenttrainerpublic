

//created by devangi pabari

import java.sql.*;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.*;
import java.rmi.*;
import java.util.*;


public class ViewResources extends HttpServlet
{
    
    protected void doGet(HttpServletRequest req,HttpServletResponse res)throws ServletException,IOException
    {
     
        res.setContentType("text/html");
        PrintWriter pw=res.getWriter();
        
        try {

            RegisterInterface stub=(RegisterInterface)Naming.lookup("rmi://localhost:5000/mcaiv");
            
            ArrayList<ArrayList<String>> arrayList= stub.viewresources();


                String docType ="<!DOCTYPE HTML\">\n";
                String title = "Message page @2";
                
               pw.println(docType + 
                    "<HTML>\n" +
                    "<HEAD>" + req.getContextPath() +
                    "<meta charset=\"utf-8\">" +
                    "<meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">" +
                    "<link rel=\"stylesheet\" href=\"https://fonts.googleapis.com/css?family=Roboto|Varela+Round\">" +
                    "<link rel=\"stylesheet\" href=\"https://fonts.googleapis.com/icon?family=Material+Icons\">"+
                    "<link rel=\"stylesheet\" href=\"https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css\">" +
                    "<link rel=\"stylesheet\" href=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css\">" +
                    "<link rel=\"stylesheet\" href=\"  "+ req.getContextPath() +"/css/StyleMantorListing.css\">" +
//out.println("<link rel='stylesheet' type='text/css' href='" + req.getContextPath() +  "/css/EmpSearchServlet.css' />");
                    //"<script src=\""+ req.getContextPath() +"/css/font-awesome.min.css\"></script>" +
//                        "<script src=\""+ req.getContextPath() +"/vendor/jquery/jquery.min.js\"></script>"+
                    "<script src=\"https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js\"></script>"+
                    "<script src=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js\"></script>"+
                    "<script src=\""+ req.getContextPath() +"/js/main.js\"></script>"+
                //"<script src=\""+ req.getContextPath() +"/js/bootstrap/bootstrap.min.js\"></script>"+
                "</HEAD>\n" +
                    "<body>" +
                        "<div class=\"container\">" +
                            "<div class=\"table-responsive\">" +
                                "<div class=\"table-wrapper\">" +
                                    "<div class=\"table-title\">" +
                                        "<div class=\"row\">" +
                                            "<div class=\"col-xs-6\">" +
                                                "<h2><b>Topic Listing</b></h2>" +
                                            "</div>" +
                                            "<div class=\"col-xs-6\">" +
                                                "<a href=\"#addTopicModal\" class=\"btn btn-success\" data-toggle=\"modal\"><i class=\"material-icons\">&#xE147;</i> <span>Add New Topic</span></a>"+    
                                            "</div>" +
                                        "</div>" +
                                    "</div>" +
                                    "<table class=\"table table-striped table-hover\">" +
                                        "<thead>" +
                                            "<tr>" +
                                                "<th>Topic Title</th>"+
                                                "<th>Description</th>" +
                                                "<th>File</th>" +
                                                "<th>Link</th>" +
                                                "<th>Edit</th>" +
                                                "<th>Deactivate</th>" +                    
                                            "</tr>" +
                                        "</thead>" +
                                        "<tbody>");
                                      
                                             String resourceid,topicTitle,Description,File,Link;
				
                                            for(int i=0;i<arrayList.size();i++){
                                            
                                                resourceid = arrayList.get(i).get(0);
                                                topicTitle = arrayList.get(i).get(1);
                                                Description = arrayList.get(i).get(2);
                                                File = arrayList.get(i).get(3);
                                                Link=arrayList.get(i).get(4);
                                                System.out.println("--------------" + resourceid);
 
                                            pw.println(                         
                                                "<tr>"+
                                                    "<td>" + topicTitle+ "</td>"+
                                                    "<td>"+Description +"</td>" +
                                                    "<td>"+ File+"</td>"+
						                            "<td>"+Link+"</td>"+                                                                    
                                                    "<td><a  onclick=\"editResourceModal("+resourceid+")\"  class=\"edit\" data-toggle=\"modal\"><i class=\"material-icons\" data-toggle=\"tooltip\" title=\"Edit\">&#xE254;</i></a></td>"+
                                                    "<td><input type=\"button\" class=\"mybutton btn btn-primary\" onclick=\"showResourceModal("+resourceid+")\" value=\"Deactivate\"></td>"+
                                                                       
                                                "</tr>");                                                
                                            }
                                           
                                            pw.println(                                                        
                                        "</tbody>"+
                                    "</table>"+
                                "</div>" +
                            "</div>"+        
                        "</div>"+
                        //<!-- Add Modal HTML -->
                       /* "<div id=\"addTopicModal\" class=\"modal fade\">"+
                            "<div class=\"modal-dialog\">"+
                                "<div class=\"modal-content\">" +
                                    "<form method=\"post\" action=\"AddTopic\" >"+
                                        "<div class=\"modal-header\">" +						
                                            "<h4 class=\"modal-title\">Add Topic</h4>"+
                                            "<button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-hidden=\"true\">&times;</button>"+
                                        "</div>"+
                                        "<div class=\"modal-body\">"+					
                                            "<div class=\"form-group\">"+
                                                "<label>Title</label>"+
                                                "<input type=\"text\" name=\"title\" class=\"form-control\" required>"+
                                            "</div>"+
                                            "<div class=\"form-group\">"+
                                                "<label>Description</label>"+
                                                "<textarea class=\"form-control\" name=\"description\" required></textarea>"+
                                            "</div>"+
                                            "<div class=\"form-group\">"+   
                                                "<label>LabeImage</label>"+
                                                //"<input type=\"file\"  name=\" "+ req.getContextPath() +"upload/images/labelImage\" id=\"labelImage\">"+
                                                "<input type=\"file\"  name=\"file\" id=\"labelImage\" accept=\"image/png, image/jpeg\">"+
                                            "</div>"+                    					
                                        "</div>"+
                                        "<div class=\"modal-footer\">"+
                                            "<input type=\"button\" class=\"btn btn-default\" data-dismiss=\"modal\" value=\"Cancel\">"+
                                            "<input type=\"submit\" class=\"btn btn-success\" value=\"Add\">"+
                                        "</div>"+
                                    "</form>"+
                                "</div>"+
                            "</div>"+
                        "</div>"+
                        */
                        //<!-- Edit Modal HTML -->
                          "<div id=\"editResourceModal\" class=\"modal fade\">"+
                            "<div class=\"modal-dialog\">"+
                                "<div class=\"modal-content\">" +
                                    "<form method=\"post\" action=\"editresource\">"+ //editresource
                                        "<div class=\"modal-header\">" +						
                                            "<h4 class=\"modal-title\">Edit Resource</h4>"+
                                            "<button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-hidden=\"true\">&times;</button>"+
                                        "</div>"+
                                        "<div class=\"modal-body\">"+		
								
                                            "<div class=\"form-group\">"+
											"<input id=\"editResource_hidden1\" type=\"hidden\" name=\"editResource_hidden1\"  class=\"form-control\" >"+
                                                "<label>Title</label>"+
                                                "<input type=\"text\" name=\"title\" class=\"form-control\" required>"+
                                            "</div>"+
                                            "<div class=\"form-group\">"+
                                                "<label>Description</label>"+
                                                "<textarea class=\"form-control\" name=\"description\" required></textarea>"+
                                            "</div>"+
                                            "<div class=\"form-group\">"+   
                                                "<label>File</label>"+
                                                "<input type=\"file\"  name=\"file\" id=\"file\">"+
                                            "</div>"+     
                                              "<div class=\"form-group\">"+   
                                                "<label>Link</label>"+
                                                "<input type=\"url\"  name=\"link\" id=\"link\">"+
                                            "</div>"+     											
                                        "</div>"+
                                        "<div class=\"modal-footer\">"+
                                            "<input type=\"button\" class=\"btn btn-default\" data-dismiss=\"modal\" value=\"Cancel\">"+
                                            "<input type=\"submit\" class=\"btn btn-info\" value=\"Save\">"+
                                        "</div>"+
                                    "</form>"+
                                "</div>"+
                            "</div>"+
                        "</div>"+
                        //<!-- Delete Modal HTML -->
                        "<div id=\"deactivateResourceModal\" class=\"modal fade\">" +
                            "<div class=\"modal-dialog\">"+
                                "<div class=\"modal-content\">"+
                                     "<form method=\"post\" action=\"deactivateresource\">"+
                                        "<div class=\"modal-header\">"+						
                                            "<h4 class=\"modal-title\">DeActive Topic</h4>"+
                                            "<button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-hidden=\"true\">&times;</button>"+
                                        "</div>"+
                                        "<div class=\"modal-body\">"+	
                                             "<input id=\"deactivate_hidden1\" type=\"hidden\" name=\"deactivate_ResourceId\" class=\"form-control\" >"+	
                                            "<p>Are you sure you want to DeActive these Topic?</p>"+
                                            "<p class=\"text-warning\"><small>This action cannot be undone.</small></p>"+
                                        "</div>"+
                                        "<div class=\"modal-footer\">"+
                                            "<input type=\"button\" class=\"btn btn-default\" data-dismiss=\"modal\" value=\"Cancel\">"+
                                            "<input type=\"submit\" class=\"btn btn-danger\" value=\"Deactive\">"+
                                        "</div>"+
                                    "</form>"+
                                "</div>"+
                            "</div>"+
                        "</div>"+
                       
                    "</body>"+
                "</html>");

           
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(e.getMessage() + "\t hello moantor");
        }

    }
}




