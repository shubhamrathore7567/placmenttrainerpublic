//MAde by Namjoshi 11/06/21


import java.io.*;

public class Question implements Serializable
{

    private String question;
    private String[]option=new String[4];
    private int ans,assestmentId,userId;
    private double weightage;

    public Question(String q,String op1,String op2,String op3,String op4,int ans,double weightage,int uId)
    {
        this.question=q;
        this.option[0]=new String(op1);
        this.option[1]=new String(op2);
        this.option[2]=new String(op3);
        this.option[3]=new String(op4);
        this.ans=ans;
        this.weightage=weightage;
        //this.assestmentId=aId;
        this.userId=uId;
    }

    public Question()
    {
        
    }
    //getter

    public String getQuestion()
    {
        return this.question;
    }

    public String getOp1()
    {
        return this.option[0];
    }
    public String getOp2()
    {
        return this.option[1];
    }
    public String getOp3()
    {
        return this.option[2];
    }
    public String getOp4()
    {
        return this.option[3];
    }

    public String getOp(int no)
    {
        return this.option[no-1];
    }
    public int getAns()
    {
        return this.ans;
    }
    public int getAssesmentID()
    {
        return this.assestmentId;
    }
    public int getUserID()
    {
        return this.userId;
    }
    public double getWeightage()
    {
        return this.weightage;
    }

     //setter

     public void setQuestion(String q)
     {
         this.question=q;
     }
 
     public void setOp1(String op1)
     {
        this.option[0]=op1;
     }
     public void setOp2(String op2)
     {
        this.option[1]=op2;
     }
     public void setOp3(String op3)
     {
        this.option[2]=op3;
     }
     public void setOp4(String op4)
     {
        this.option[3]=op4;
     }
     public void setAns(int ans)
     {
        this.ans=ans;
     }
     public void setAssesmentID(int aid)
     {
         this.assestmentId=aid;
     }
     public void setUserID(int uid)
     {
        this.userId=uid;
     }
     public void setWeightage(double w)
     {
        this.weightage=w;
     }
}