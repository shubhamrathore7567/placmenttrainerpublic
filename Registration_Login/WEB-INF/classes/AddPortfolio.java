//shubham 2021-06-13 added

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.rmi.*;
import java.util.ArrayList;

public class AddPortfolio extends HttpServlet {
    public void doPost(HttpServletRequest req,HttpServletResponse res)throws ServletException,IOException
    {
        try{
            res.setContentType("text/html");
            RegisterInterface stub=(RegisterInterface)Naming.lookup("rmi://localhost:5000/mcaiv");
            PrintWriter pw=res.getWriter();
            PortfolioView portfolio = new PortfolioView();
            portfolio = stub.GetPortfolio(1);
            if(portfolio != null)
            {
                res.sendRedirect("ViewPortfolio");
            }
            portfolio.UserId = 1;
            portfolio.Profession = req.getParameter("profession");
            System.out.println(portfolio.Profession);
            portfolio.Age = req.getParameter("age");
            System.out.println(portfolio.Age);
            portfolio.HighestEducation = req.getParameter("highestEducation");
            System.out.println(portfolio.HighestEducation);
            portfolio.ProjectDescription = req.getParameter("projectDescription");
            portfolio.ProjectTitle = req.getParameter("projectTitle");
            portfolio.BaseLanguage = req.getParameter("baseLanguage");
            portfolio.BackendTech = req.getParameter("backendTech");
            portfolio.FrontendTech = req.getParameter("frontendTech");
            portfolio.Dbms = req.getParameter("dbms");
            String[] Sems = req.getParameterValues("sem");
            String[] universities = req.getParameterValues("university");
            String[] passingYears = req.getParameterValues("passingYear");
            String[] percantages = req.getParameterValues("percentage");
            portfolio.EducationDetailList = new ArrayList<EducationDetail>();
            for(int index=0;index<Sems.length;index++)
            {
                EducationDetail education = new EducationDetail();
                education.Semister = Sems[index];
                education.Degree = universities[index];
                education.PassingYear = passingYears[index];
                education.Percentage = percantages[index];
                portfolio.EducationDetailList.add(education);
            }
            stub.AddPortfolio(portfolio);
            res.sendRedirect("ViewPortfolio");
            pw.close();
        }
        catch(Exception ex)
        {
            System.out.println(ex.getMessage());
        }
    }
}
