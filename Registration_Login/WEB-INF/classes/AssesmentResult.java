//MAde by Namjoshi 09/06/21


import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.rmi.*;
import javax.servlet.http.*;
import java.io.*;  
import java.lang.Math;


public class AssesmentResult extends HttpServlet
{
    protected void doPost(HttpServletRequest req,HttpServletResponse res)throws ServletException,IOException
    {
     
        res.setContentType("text/html");

        HttpSession session=req.getSession(false); 
        
        Assesment asses=(Assesment)session.getAttribute("AssesmentObjact");

        int userAns[]= new int[asses.que.size()];
        double studentMarks=0,studentPercentage=0;

        String html = "<!DOCTYPE html>"+
        "<html lang='en'>"+
        "<head>"+
        "<meta charset='UTF-8'>"+
        "<meta http-equiv='X-UA-Compatible' content='IE=edge'>"+
        "<meta name='viewport' content='width=device-width, initial-scale=1.0'>"+
        "<title>Portfolio</title>"+
        "<link rel='stylesheet' href='https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback'>"+
        "<link rel='stylesheet' href='./dist/css/adminlte.css'>"+
        "<link rel='stylesheet' href='./plugins/fontawesome-free/css/all.css'>"+
        "<link rel='stylesheet' href='./dist/css/custom.css'>"+
        "</head>"+
        "<body class='hold-transition layout-top-nav'>"+
        "<div class='wrapper'>"+
        "<nav class='main-header navbar navbar-expand-md navbar-light navbar-white' style='background-color:#125d98;'>"+
        "<a href='../../index3.html' class='navbar-brand'>"+
          
          "<span class='brand-text font-weight-light'>Online Prep</span>"+
        "</a>"+
  
        "<button class='navbar-toggler order-1' type='button' data-toggle='collapse' data-target='#navbarCollapse' aria-controls='navbarCollapse' aria-expanded='false' aria-label='Toggle navigation'>"+
          "<span class='navbar-toggler-icon'></span>"+
        "</button>"+
  
        "<div class='collapse navbar-collapse order-3' id='navbarCollapse'>"+
          "<ul class='navbar-nav ml-auto'>"+
            "<li class='nav-item'>"+
              "<a href='index.html' class='nav-link'>Home</a>"+
            "</li>"+
            "<li class='nav-item'>"+
              "<a href='Topics.html' class='nav-link'>Topics</a>"+
            "</li>"+
            "<li class='nav-item'>"+
              "<a href='Mentors.html' class='nav-link'>Mentors</a>"+
            "</li>"+
            "<li class='nav-item'>"+
              "<a href='Friends.html' class='nav-link'>Friends</a>"+
            "</li>"+
            "<li class='nav-item'>"+
              "<a href='Contact-us.html' class='nav-link'>Contact us</a>"+
            "</li>"+
            "<li class='nav-item'>"+
              "<a href='#' class='nav-link'>Contact</a>"+
            "</li>"+
            "<li class='nav-item dropdown'>"+
              "<a id='dropdownSubMenu1' href='#' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false' class='nav-link dropdown-toggle'>"+
                "<i class='fas fa-user mr-2'></i>"+
              "</a>"+
              "<ul aria-labelledby='dropdownSubMenu1' class='dropdown-menu border-0 shadow'>"+
                "<li><a href='Profile.html' class='dropdown-item'>Profile </a></li>"+
                "<li><a href='change-pswd.html' class='dropdown-item'>Change Password</a></li>"+
  
                "<li class='dropdown-divider'></li>"+
                "<li><a href='Profile.html' class='dropdown-item'>Logout</a></li>"+
              "</ul>"+
            "</li>"+
          "</ul>"+
        "</div>"+
    "</nav>"+
"</div>"+
"<div class='content-wrapper pt-2'>"+
    "<div class='container'>";


    String footer="</div>"+
    "</div>"+
    " <footer class='main-footer' style='background-color:black;'>"+
        "<center>"+
          "<strong>Copyright &copy; 2014-2021 <a href='#'>OPR</a>.</strong> All rights reserved."+
        "</center>"+
      "</footer>"+
    "<script src='./plugins/jquery/jquery.min.js'></script>"+
    "<script src='./plugins/bootstrap/js/bootstrap.bundle.js'></script>"+
    "<script src='./dist/js/adminlte.js'></script>"+
    "<script src='./dist/js/demo.js'></script>"+
"</body>"+
"</html>";

        for(int i=0;i<userAns.length;i++)
        {
            userAns[i]=Integer.parseInt(req.getParameter("o"+i));
            System.out.println("userANs ="+userAns[i]+"\tans= "+asses.que.get(i).getAns());
            
            if(userAns[i]==asses.que.get(i).getAns())
            {
                studentMarks+=asses.que.get(i).getWeightage();
            }
        }
        studentPercentage=(100*studentMarks)/(asses.getWeightage());

        PrintWriter pw=res.getWriter();
        
    
        pw.println(html);
        pw.println("<h1>Result</h1><br><br>");

        pw.println("<b>Marks= "+studentMarks+"</b>");
        pw.println("<b><br>Percentage= "+Math.round(studentPercentage)+" %</b>");



        pw.println("<br><br><h3><b>Correct Answers</b></h3>");

        pw.println("<table class=\"table table-striped table-hover\">" +
                                        "<thead>" +
                                            "<tr>" +
                                                "<th>No</th>"+
                                                "<th>Question</th>" +
                                                "<th>Answer</th>" +                   
                                            "</tr>" +
                                        "</thead>" +
                                        "<tbody>");

        for(int i=0;i<asses.que.size();i++)
        {
            pw.println(                         
                "<tr>"+
                    "<td>" +(i+1)+"</td>"+
                    "<td>"+asses.que.get(i).getQuestion() +"</td>" +
                    "<td>"+asses.que.get(i).getOp(asses.que.get(i).getAns())+"</td>" +       
                    "</tr>");

        }

        pw.println("</tbody></table>");


        pw.println("</h2>"+footer);
           

    }
}
