//MAde by Namjoshi 11/06/21  /


import java.sql.*;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.rmi.*;
import java.util.*;
import javax.servlet.http.*;


public class AssesmentListing extends HttpServlet
{
    
    protected void doGet(HttpServletRequest req,HttpServletResponse res)throws ServletException,IOException
    {
     



        String html = "<!DOCTYPE html>"+
            "<html lang='en'>"+
            "<head>"+
            "<meta charset='UTF-8'>"+
            "<meta http-equiv='X-UA-Compatible' content='IE=edge'>"+
            "<meta name='viewport' content='width=device-width, initial-scale=1.0'>"+
            "<title>Portfolio</title>"+
            "<link rel='stylesheet' href='https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback'>"+
            "<link rel='stylesheet' href='./dist/css/adminlte.css'>"+
            "<link rel='stylesheet' href='./plugins/fontawesome-free/css/all.css'>"+
            "<link rel='stylesheet' href='./dist/css/custom.css'>"+
            "</head>"+
            "<body class='hold-transition layout-top-nav'>"+
            "<div class='wrapper'>"+
            "<nav class='main-header navbar navbar-expand-md navbar-light navbar-white' style='background-color:#125d98;'>"+
            "<a href='../../index3.html' class='navbar-brand'>"+
              
              "<span class='brand-text font-weight-light'>Online Prep</span>"+
            "</a>"+
      
            "<button class='navbar-toggler order-1' type='button' data-toggle='collapse' data-target='#navbarCollapse' aria-controls='navbarCollapse' aria-expanded='false' aria-label='Toggle navigation'>"+
              "<span class='navbar-toggler-icon'></span>"+
            "</button>"+
      
            "<div class='collapse navbar-collapse order-3' id='navbarCollapse'>"+
              "<ul class='navbar-nav ml-auto'>"+
                "<li class='nav-item'>"+
                  "<a href='index.html' class='nav-link'>Home</a>"+
                "</li>"+
                "<li class='nav-item'>"+
                  "<a href='Topics.html' class='nav-link'>Topics</a>"+
                "</li>"+
                "<li class='nav-item'>"+
                  "<a href='Mentors.html' class='nav-link'>Mentors</a>"+
                "</li>"+
                "<li class='nav-item'>"+
                  "<a href='Friends.html' class='nav-link'>Friends</a>"+
                "</li>"+
                "<li class='nav-item'>"+
                  "<a href='Contact-us.html' class='nav-link'>Contact us</a>"+
                "</li>"+
                "<li class='nav-item'>"+
                  "<a href='#' class='nav-link'>Contact</a>"+
                "</li>"+
                "<li class='nav-item dropdown'>"+
                  "<a id='dropdownSubMenu1' href='#' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false' class='nav-link dropdown-toggle'>"+
                    "<i class='fas fa-user mr-2'></i>"+
                  "</a>"+
                  "<ul aria-labelledby='dropdownSubMenu1' class='dropdown-menu border-0 shadow'>"+
                    "<li><a href='Profile.html' class='dropdown-item'>Profile </a></li>"+
                    "<li><a href='change-pswd.html' class='dropdown-item'>Change Password</a></li>"+
      
                    "<li class='dropdown-divider'></li>"+
                    "<li><a href='Profile.html' class='dropdown-item'>Logout</a></li>"+
                  "</ul>"+
                "</li>"+
              "</ul>"+
            "</div>"+
        "</nav>"+
    "</div>"+
    "<div class='content-wrapper pt-2'>"+
        "<div class='container'>";


        String footer="</div>"+
        "</div>"+
        " <footer class='main-footer' style='background-color:black;'>"+
            "<center>"+
              "<strong>Copyright &copy; 2014-2021 <a href='#'>OPR</a>.</strong> All rights reserved."+
            "</center>"+
          "</footer>"+
        "<script src='./plugins/jquery/jquery.min.js'></script>"+
        "<script src='./plugins/bootstrap/js/bootstrap.bundle.js'></script>"+
        "<script src='./dist/js/adminlte.js'></script>"+
        "<script src='./dist/js/demo.js'></script>"+
    "</body>"+
    "</html>";





        res.setContentType("text/html");
        //int topicId = 1 ;   //fetch later
    

    
        int topicId =Integer.parseInt(req.getQueryString()); 

        System.out.println("Query String value: "+topicId);
        

        HttpSession session = req.getSession();

        session.setAttribute("topicid", topicId);
        
        String userRole=(String)session.getAttribute("userrole");

        PrintWriter pw=res.getWriter();
        
        try 
        {

            pw.println(html);
            RegisterInterface stub=(RegisterInterface)Naming.lookup("rmi://localhost:5000/mcaiv");
            
            ArrayList<Assesment> assesment= stub.getAssesment(topicId);

            if(assesment!=null)
            {
                
                String docType ="<!DOCTYPE HTML\">\n";
                String title = "Assesment Listing";
                
               pw.println(docType + 
                    "<HTML>\n" +
                    "<HEAD><TITLE>" + title +
                    "</TITLE>"+
                    "<script src=\"https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js\"></script>" +
                    "<script src=\"https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js\"></script>" +
                    "<script src=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js\"></script>"+
                    "</HEAD>\n" +
                    "<BODY>\n" +
                    "<div class=\"container\">" +
                    "<br><br><h2>Assesments</h2>");
                    if(userRole.equals("Mentor"))  //for mentor
                    {
                        pw.println("<button onclick=\"location.href='addQuestions.html';\">Add Assesment</button>");
                    }
                    pw.println("<TABLE Class=\"table table-hover\"><THEAD><TR><TH scope =\"col\">No.</TH><TH scope =\"col\">Title</TH></TR><TBODY>");
                    
        
                
                String topicTitle,Description,LabelImage;

                int assesmentId;
                for(int i=0;i<assesment.size();i++)
                {
                    assesmentId=assesment.get(i).getAssesmentID();

                        pw.println( 
                          "<TR><TD>"+ (i+1)+" </TD><TD>" + String.valueOf(assesment.get(i).getTitle()) + "</TD>");
                        if(userRole.equals("Student"))//for student
                        {
                            //pw.println("<TD><button class=\"col-sm-3 btn btn-primary\" onclick=\"location.href='TakeAssesment?';\">give Assesment</button></TD>");
                            pw.println("<TD><button class=\"col-sm-3 btn btn-primary\" onclick=\"location.href='TakeAssesment?"+assesmentId+"';\">give Assesment</button></TD>");
                        }
                        
                        pw.println("</TR><br>");
                }
                pw.println("<TBODY></TABLE></DIV>");

                pw.println(footer);
            
            }

           
        } 
        catch (Exception e) 
        {
            e.printStackTrace();
            System.out.println(e.getMessage() + "\t hello");
        }

    }


}




