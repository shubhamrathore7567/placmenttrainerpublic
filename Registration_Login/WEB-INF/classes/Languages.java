
// made by shivangi chotaliya 10/6/2021

import java.io.*;

public class Languages implements Serializable
{
    private int languageID;
    private String languageName;

    public Languages(int languageID,String languageName)
    {
        this.languageID=languageID;
        this.languageName=languageName;
    }
    public void setLanguageID(int languageID)
    {
        this.languageID=languageID;
    }
    public int getLanguageID()
    {
        return this.languageID;
    }

    public void setLanguageName(String languageName)
    {
        this.languageName=languageName;
    }

    public String getLanguageName(String languageName)
    {
        return languageName;
    }
   
   
}

