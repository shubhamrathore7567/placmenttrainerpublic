--shubhamrathore 2021-06-10 added
CREATE TABLE PlacementTrainer_DB.TopicMaster(
	TopicId int auto_increment primary key,
    userId int,
    LanguageId int,
    Title varchar(50),
    Description varchar(500),
    LabelImage nvarchar(500),
    createdAt Datetime,
    CreatedBy int,
    ModifiedAt Datetime,
    ModifiedBy int,
    FOREIGN KEY (userId) REFERENCES UserMaster(userId),
    FOREIGN KEY (LanguageId) REFERENCES Languages(LanguageID)
);