CREATE TABLE PlacementTrainer_DB.Resources(
	ResourceId int auto_increment primary key,
    TopicId int,
    Title varchar(200),
    Description varchar(500),
    Attachement varchar(1000),
    LinkToResource varchar(500), 
    CreatedBy int,
    CreatedAt Datetime,
    ModifiedBy int,
    ModifiedAt Datetime,
    FOREIGN KEY (TopicId) REFERENCES TopicMaster(TopicId)
);