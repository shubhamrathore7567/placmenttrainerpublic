--shubhamrathore 2021-06-10 added
CREATE TABLE PlacementTrainer_DB.UserTopicTransaction(
	userId int,
    TopicId int,
    createdAt Datetime,
    CreatedBy int,
    ModifiedAt Datetime,
    ModifiedBy int,
    FOREIGN KEY (userId) REFERENCES UserMaster(userId),
    FOREIGN KEY (TopicId) REFERENCES TopicMaster(TopicId)
);