--shivangi 2021-06-10 added
CREATE TABLE PlacementTrainer_DB.PortfolioMaster(
	PortfolioId int auto_increment primary key,
    userId int,
    Skills varchar(100),
    ProfilePic varchar(500),
    createdAt Datetime,
    CreatedBy int,
    ModifiedAt Datetime,
    ModifiedBy int,
    FOREIGN KEY (userId) REFERENCES UserMaster(userId)
);
Alter TABLE PlacementTrainer_DB.PortfolioMaster ADD Column Profession varchar(100);
Alter TABLE PlacementTrainer_DB.PortfolioMaster ALTER Column Age decimal(3,2);
Alter TABLE PlacementTrainer_DB.PortfolioMaster ADD Column HighestEducation varchar(200);