
--shivangi 2021-06-10 added
--Removed isShufflable bit,updated weightage,ans datatype column Namjoshi 11/6

CREATE TABLE PlacementTrainer_DB.Questions(
	ID int auto_increment primary key,
    AssesmentID int,
    UserID int,
    Question varchar(200),
    Option1 varchar(50),
    Option2 varchar(50),
    Option3 varchar(50),
    Option4 varchar(50),
    Answer int,
    Weightage decimal(10,2), 
    CreatedBy int,
    CreatedAt Datetime,
    ModifiedBy int,
    ModifiedAt Datetime,
     FOREIGN KEY (userId) REFERENCES UserMaster(userId)
);