--shivangi 2021-06-10 added
CREATE TABLE PlacementTrainer_DB.FinalYearProjectDetail(
	ProjectId int auto_increment primary key,
    PortfolioId int,
    ProjectTitle varchar(100),
    ProjectDescription varchar(500),
    BaseLanguage varchar(200), 
    FrontendTech varchar(200), 
    BackendTech varchar(200), 
    DBMS varchar(200),
    CreatedBy int,
    CreatedAt Datetime,
    ModifiedBy int,
    ModifiedAt Datetime,
    FOREIGN KEY (PortfolioId) REFERENCES PortfolioMaster(PortfolioId)
);