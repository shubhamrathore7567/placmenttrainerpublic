--shubham rathore 2021-06-09 added
CREATE TABLE PlacementTrainer_DB.StudentProfile(
	profileId int auto_increment primary key,
    userId int,
    organization varchar(200),
    stream varchar(200),
    FOREIGN KEY (userId) REFERENCES UserMaster(userId)
);

ALTER TABLE PlacementTrainer_DB.StudentProfile ADD COLUMN FullName varchar(200);
ALTER TABLE PlacementTrainer_DB.StudentProfile ADD COLUMN CreatedAt datetime;
ALTER TABLE PlacementTrainer_DB.StudentProfile ADD COLUMN ModifiedAt datetime;
ALTER TABLE PlacementTrainer_DB.StudentProfile ADD COLUMN CreatedBy int;
ALTER TABLE PlacementTrainer_DB.StudentProfile ADD COLUMN ModifiedBy int;