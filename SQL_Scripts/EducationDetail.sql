--shubhamrathore 2021-06-10 added
CREATE TABLE PlacementTrainer_DB.EducationDetail(
	DetailId int auto_increment primary key,
    PortfolioId int,
    Sem varchar(20),
    Marks decimal(3,2),
    Percantage decimal(3,2),
    Degree varchar(200),
    createdAt Datetime,
    CreatedBy int,
    ModifiedAt Datetime,
    ModifiedBy int,
    FOREIGN KEY (PortfolioId) REFERENCES PortfolioMaster(PortfolioId)
);

Alter TABLE PlacementTrainer_DB.EducationDetail Drop Column Marks;
Alter TABLE PlacementTrainer_DB.EducationDetail ADD Column PassingYear varchar(5);