--shubham rathore 2021-06-09 added
CREATE TABLE PlacementTrainer_DB.UserMaster(
	userId int auto_increment primary key,
    username varchar(20),
    name varchar(50),
    password varchar(100),
    email varchar(50),
    contact varchar(12),
    isActive bit default 1,
    userRole varchar(7),
    createdAt Datetime,
    CreatedBy int,
    ModifiedAt Datetime,
    ModifiedBy int
);