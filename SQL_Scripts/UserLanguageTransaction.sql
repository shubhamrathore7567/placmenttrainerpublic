--shubhamrathore 2021-06-10 added
CREATE TABLE PlacementTrainer_DB.UserLanguageTransection(
	LanguageId int,
    userId int,
    createdAt Datetime,
    CreatedBy int,
    ModifiedAt Datetime,
    ModifiedBy int,
    FOREIGN KEY (LanguageId) REFERENCES Languages(LanguageID),
    FOREIGN KEY (userId) REFERENCES UserMaster(userId)
);