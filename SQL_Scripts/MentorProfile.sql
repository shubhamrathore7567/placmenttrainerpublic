--shubham rathore 2021-06-09 added
CREATE TABLE PlacementTrainer_DB.MentorProfile(
	profileId int auto_increment primary key,
    userId int,
    skills varchar(500),
    FOREIGN KEY (userId) REFERENCES UserMaster(userId)
);