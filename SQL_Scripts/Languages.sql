--shubham rathore 2021-06-10 added
CREATE TABLE PlacementTrainer_DB.Languages(
	LanguageID int auto_increment primary key,
    LanguageName varchar(50),
    createdAt Datetime,
    CreatedBy int,
    ModifiedAt Datetime,
    ModifiedBy int
);